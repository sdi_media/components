<?php

namespace SDI\ComponentsBundle\EventListener;

use JMS\Serializer\Serializer;
use SDI\ComponentsBundle\Component\Response\RestResponse;
use SDI\ComponentsBundle\Exception\RestException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Kernel;

class ResponseListener
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var
     */
    private $envTag;

    /**
     * ResponseListener constructor.
     * @param Serializer $serializer
     * @param Kernel $kernel
     */
    public function __construct(Serializer $serializer, Kernel $kernel)
    {
        $this->serializer = $serializer;
        $this->envTag = $kernel->getEnvironment();
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $request = $event->getRequest();
        $exception = $event->getException();
        if ($exception instanceof RestException) {
            $response = new RestResponse();

            $msg = $exception->getMessage();

            if ($exception->getCustomErrorCode()) {
                $response->setErrorCode($exception->getCustomErrorCode());
            }

            if ($exception->getPrevious() instanceof \Doctrine\DBAL\DBALException) {
                $msg = 'Database error';
            }

            $response->setStatus(RestResponse::ERROR)
                ->setMessage($msg)
                ->setFormat($request->getRequestFormat());

            $trace = null;
            if ($this->envTag == 'dev') {
                $trace = $exception->getTraceAsString();
            }

            if ($exception instanceof HttpExceptionInterface) {
                $response->setStatusCode($exception->getStatusCode());
                $response->headers->replace($exception->getHeaders());
            } else {
                if ($exception->getCode()) {
                    $response->headers->replace(['X-Status-Code' => $exception->getCode()]);
                    $response->setStatusCode($exception->getCode());
                } else {
                    $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
                }
            }

            $response->buildData($trace)
                ->serializeData($this->serializer);

            $event->setResponse($response->getResponse());
        }
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response = $event->getResponse();
        $request = $event->getRequest();

        if ($response instanceof RestResponse) {
            $response->setFormat($request->getRequestFormat())
                ->buildData()
                ->serializeData($this->serializer);
            $event->setResponse($response->getResponse());
        }
    }
}