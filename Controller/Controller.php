<?php

namespace SDI\ComponentsBundle\Controller;

use Doctrine\ORM\EntityManager;
use Sluggable\Fixture\Handler\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use JMS\Serializer\SerializationContext;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\User\UserInterface;

class Controller extends BaseController
{
    /**
     * @var EntityManager
     */
    protected $em = null;

    /**
     * Get Entity Manager
     *
     * @return EntityManager
     */
    public function getEm()
    {
        if ($this->em == null) {
            $this->em = $this->get('doctrine.orm.entity_manager');
        }
        return $this->em;
    }

    /**
     * @return SecurityContext
     */
    protected function getSecurityContext()
    {
        return $this->container->get('security.context');
    }

    /**
     * @return UserInterface|null
     */
    public function getUser()
    {
        $token = $this->container->get('security.token_storage')->getToken();
        if ($token) {
            return $token->getUser();
        }

        return null;
    }

    /**
     * @return FlashBagInterface
     */
    protected function getSessionFlashBag()
    {
        return $this->get('session')->getFlashBag();
    }

    /**
     * @param mixed $data
     * @param int $code
     * @param string $type
     * @param SerializationContext $context
     * @return Response
     */
    public function serializedResponse($data, $code = 200, $type = 'json', SerializationContext $context = null)
    {
        return $this->container->get('sdi_components.response_serialized')->response($data, $code, $context, $type);
    }

    /**
     * @param mixed $data
     * @param int $code
     * @param SerializationContext $context
     * @return Response
     */
    public function serializedFormatResponse($data, $code = 200, SerializationContext $context = null)
    {
        return $this->container->get('sdi_components.response_serialized')->formatResponse($data, $code, $context);
    }

    /**
     * @return \SDI\ComponentsBundle\Component\Response\SerializedBuilder
     */
    public function getResponseSerializedBuilder()
    {
        return $this->container->get('sdi_components.response_serialized_builder');
    }

    /**
     * Get mailer
     *
     * @return object
     */
    public function getMailer()
    {
        return $this->container->get('mailer');
    }

    /**
     * Convert form errors to array
     *
     * @param Form $form
     * @return array
     */
    protected function getFormErrors($form)
    {
        $errors = array();
        if ($form instanceof Form) {
            foreach ($form->getErrors() as $error) {
                $code = $error->getCause()->getCode();
                if (isset($errors[$code])) {
                    $errors[uniqid($code)] = $error->getMessage();
                } else {
                    $errors[$code] = $error->getMessage();
                }
            }

            foreach ($form->all() as $key => $child) {
                $childErrors = $this->getFormErrors($child);
                if ($childErrors) {
                    $errors[$key] = $childErrors;
                }
            }
        }

        return $errors;
    }
}
