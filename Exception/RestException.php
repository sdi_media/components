<?php

namespace SDI\ComponentsBundle\Exception;


class RestException extends \RuntimeException
{
    /**
     * @var int
     */
    private $customErrorCode = 0;

    /**
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     * @param int $errorCode
     */
    public function __construct($message = '', $code = 0, \Exception $previous = null, $errorCode = 0)
    {
        if ($errorCode) { echo $this->message;
            $this->customErrorCode = $errorCode;
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return int
     */
    public function getCustomErrorCode()
    {
        return $this->customErrorCode;
    }
}