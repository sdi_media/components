<?php

namespace SDI\ComponentsBundle\Component\Uxml;

use SDI\ComponentsBundle\Component\Response\Serialized;
use SDI\ComponentsBundle\Component\Uxml\Model\Character;
use SDI\ComponentsBundle\Component\Uxml\Model\Character\Actor;
use SDI\ComponentsBundle\Component\Uxml\Model\Dialog;
use SDI\ComponentsBundle\Component\Uxml\Model\Song;
use Symfony\Component\CssSelector\CssSelector;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

class Crawler54
{
    /**
     * @var string raw dsx file content
     */
    protected $rawXml;

    /**
     * @var Crawler crawler containing full dsx file
     */
    protected $dsxCrawler;

    /**
     * @var Crawler crawler containing dialogs (whether from sourceText or job part)
     */
    protected $dialogCrawler;

    /**
     * @var array extracted project properties
     */
    protected $properties;

    /**
     * @var Character[] extracted characters
     */
    protected $characters;

    /**
     * @var Song[] extracted characters
     */
    protected $songs;

    /**
     * @var Dialog[] extracted dialogs
     */
    protected $dialogs;

    /**
     * @var bool whether use source translation
     */
    protected $fromSource;

    /**
     * @var string target language
     */
    protected $language;

    /**
     * @var Serialized
     */
    private $serializedResponse;

    /**
     * @var bool
     */
    private $clearText = true;

    /**
     * @param Serialized $serializedResponse \
     */
    public function __construct(Serialized $serializedResponse)
    {
        $this->serializedResponse = $serializedResponse;
    }

    /**
     * @param string $rawXml raw dsx file content
     * @param array $params currently used: from_source, language
     */
    public function parse($rawXml, $params = array(), $clearText = true)
    {
        $this->rawXml = $rawXml;
        $this->fromSource = isset($params['from_source']) ? $params['from_source'] : false;
        $this->language = isset($params['language']) ? $params['language'] : null;
        $this->dsxCrawler = new Crawler();
        $this->clearText = $clearText;

        CssSelector::disableHtmlExtension();
        $this->dsxCrawler->addXmlContent($rawXml);

        $this->dialogCrawler = $this->extractDialogCrawler();

        $this->parseProperties();
        $this->parseCharacters();
        $this->parseDialogs();
    }

    /**
     * @return array get array with project properties
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @return Character[] get characters
     */
    public function getCharacters()
    {
        return $this->characters;
    }

    /**
     * @return Song[] get songs
     */
    public function getSongs()
    {
        return $this->songs;
    }

    /**
     * @return string
     */
    public function getSongsAsJson()
    {
        $songs = array_map('array_values', array($this->songs));
        return json_encode(end($songs));
    }

    /**
     * @return string
     */
    public function getCharactersAsJson()
    {
        $characters = array_map('array_values', array($this->characters));
        return json_encode(end($characters));
    }

    /**
     * @return string
     */
    public function getDialogsAsJson()
    {
        $dialogs = array_map('array_values', array($this->dialogs));
        return json_encode(end($dialogs));
    }

    /**
     * @return Dialog[] get dialogs
     */
    public function getDialogs()
    {
        return $this->dialogs;
    }

    /**
     * @return bool true whether crawler parse source dialogs or false when specified translation
     */
    public function isFromSource()
    {
        return $this->fromSource;
    }

    /**
     * parse project properties to array
     */
    protected function parseProperties()
    {
        $propertiesCrawler = $this->dsxCrawler->filter('default|project default|properties');
        $projectTitleElement = $propertiesCrawler->filter('default|projectTitle');
        $masterTitleElement = $propertiesCrawler->filter('default|masterTitle');
        $masterClientElement = $propertiesCrawler->filter('default|masterClient');
        $seasonNumberElement = $propertiesCrawler->filter('default|seasonNumber');
        $episodeNumberElement = $propertiesCrawler->filter('default|episodeNumber');
        $frameRateElement = $propertiesCrawler->filter('default|frameRate');

        $translatedTitleElement = $this->dialogCrawler->filter('default|localProjectTitle');
        $translatedSeriesTitleElement = $this->dialogCrawler->filter('default|localMasterTitle');

        $properties = array(
            'projectTitle' => count($projectTitleElement) ? $projectTitleElement->text() : null,
            'translatedTitle' => count($translatedTitleElement) ? $translatedTitleElement->text() : null,
            'translatedSeriesTitle' => count($translatedSeriesTitleElement) ? $translatedSeriesTitleElement->text() : null,
            'masterTitle' => count($masterTitleElement) ? $masterTitleElement->text() : null,
            'masterClient' => count($masterClientElement) ? $masterClientElement->text() : null,
            'seasonNumber' => count($seasonNumberElement) ? $seasonNumberElement->text() : null,
            'episodeNumber' => count($episodeNumberElement) ? $episodeNumberElement->text() : null,
            'languageSymbol' => $this->dialogCrawler->attr('targetLang'),
            'frameRate' => count($frameRateElement) ? (int)$frameRateElement->text() : null,
        );

        if ($this->fromSource) {
            $properties['languageSymbol'] = $propertiesCrawler->filter('default|sourceLanguage')->text();
        }

        $translatorCrawler = $this->dialogCrawler->filter('sdi|translator');
        $properties['translatedBy'] = count($translatorCrawler) ? $translatorCrawler->text() : '';

        $createdCrawler = $this->dialogCrawler->filter('default|created');
        $properties['createdDate'] = count($createdCrawler) ? $createdCrawler->attr('date') : '';
        $properties['createdTime'] = count($createdCrawler) ? $createdCrawler->attr('time') : '';

        $sourceLanguageCrawler = $propertiesCrawler->filter('default|sourceLanguage');
        $sourceLanguageCrawler->each(function (Crawler $node, $i) use (&$properties) {
            if ($node->attr('main') == 'true') {
                $properties['sourceLanguage'] = $node->text();
            } else if ($node->attr('main') == 'false') {
                $properties['targetLanguage'] = $node->text();
            }
        });

        if (isset($properties['sourceLanguage'])) {
            $properties['languageSymbol'] = $properties['sourceLanguage'];
        }

        $this->properties = $properties;
    }

    protected function parseCharacters()
    {
        $characters = array();

        if ($this->isFromSource()) {
            $entityCrawler = $this->dsxCrawler->filter('default|properties default|entities default|entity');
        } else {
            $entityCrawler = $this->dialogCrawler->filter('default|entities default|entity');
        }

        $tmpEntityCrawler = new Crawler($entityCrawler->getNode(0));

        $entityCrawler->each(function (Crawler $node, $i) use (&$characters, $tmpEntityCrawler) {
            if ('character' != $node->attr('type')) {
                return true;
            }
            if (count($node->filter('default|firstName'))) {
                $name = sprintf('%s %s', $node->filter('default|firstName')->text(), $node->filter('default|lastName')->text());
            } elseif (count($node->filter('default|name'))) {
                $name = sprintf('%s', $node->filter('default|name')->text());
            } else {
                $name = '';
            }

            $characterId = $node->attr('id');

            $character = new Character();
            $character
                ->setId($characterId)
                ->setGender($node->attr('gender'))
                ->setRoleType($node->attr('characterType'))
                ->setType($node->attr('type'))
                ->setAge($node->attr('age'))
                ->setName($name);

            // try to find corresponding actor entry
            if (!$this->isFromSource()) {
                $sdiCharacterCrawler = $this->dsxCrawler->filter(sprintf('sdi|character[entityId="%s"]', $characterId));
                if (count($sdiCharacterCrawler)) {
                    $agentCrawler = $sdiCharacterCrawler->parents()->first();
                    $agentEntityId = $agentCrawler->attr('entityId');
                    $personCrawler = $tmpEntityCrawler->filter(sprintf('default|entity[id=""]', $agentEntityId));

                    if (count($personCrawler)) {
                        if (count($node->filter('default|firstName'))) {
                            $actorName = sprintf('%s %s', $node->filter('default|firstName')->text(), $node->filter('default|lastName')->text());
                        } else {
                            $actorName = sprintf('%s', $node->filter('default|name')->text());
                        }
                        $actor = new Actor();
                        $actor
                            ->setId($agentEntityId)
                            ->setName($actorName);

                        $character->setActor($actor);
                    }
                }
            }

            $characters[$characterId] = $character;
        });

        if ($this->isFromSource()) {
            $translatedNamesCrawler = $this->dsxCrawler->filter('default|properties default|entities default|translatedName');
        } else {
            $translatedNamesCrawler = $this->dialogCrawler->filter('default|entities default|translatedName');
        }

        $translatedNamesCrawler->each(function (Crawler $node, $i) use (&$characters) {
            /** @var $character Character */
            if (isset($characters[$node->attr('entityId')])) {
                $character = $characters[$node->attr('entityId')];
                if ($character instanceof Character) {
                    $character->setTranslatedName($node->text());
                }
            }
        });

        $this->characters = $characters;
    }

    /**
     * @return Crawler
     * @throws \Exception
     */
    public function extractDialogCrawler()
    {
        if ($this->isFromSource()) {
            $dialogCrawler = $this->dsxCrawler->filter('default|properties default|sourceText');
        } else {
            $language = $this->language;

            $jobsCrawler = $this->dsxCrawler->filter('default|job');
            $jobsCrawler = $jobsCrawler->reduce(function (Crawler $node, $i) use ($language) {
                // closure should return false when job is for another language then desired
                return ($node->attr('targetLang') != $language) ? false : true;
            });

            $dialogCrawler = $jobsCrawler->first();
        }

        if (0 === count($dialogCrawler)) {
            throw new \Exception($this->isFromSource() ? 'Dialogs from source text was not found' : sprintf('Job for language %s was not found', $this->language), Response::HTTP_BAD_REQUEST);
        }

        return $dialogCrawler;
    }

    /**
     * parses dialogs from dsx file to their respective models
     */
    public function parseDialogs()
    {
        $dialogNodes = $this->dialogCrawler->filter('tt|p');

        $characters = $this->characters;
        $dialogs = array();

        $dialogNodes->each(function (Crawler $node, $i) use (&$characters, &$dialogs) {
            $dialog = new Dialog();
            $dialogId = $node->attr('sdi:id');
            $dialog->setLineNumber($i + 1);

            $dialog
                ->setId($dialogId)
                ->setRole($node->attr('ttm:role'))
                ->setTcIn($node->attr('begin'))
                ->setTcOut($node->attr('end'))
                ->setIsSource($this->isFromSource());

            $contentNode = new Crawler($node->children()->getNode(0));

            if ($contentNode->count() < 1 || $contentNode->children()->count() === 0) {
                $contentNode = new Crawler();
                $contentNode->addXmlContent('<tt:span>'. $node->html() .'</tt:span>');
            }
            
            $contentNode->registerNamespace('tt', 'https://www.w3.org/ns/ttml/');

            $children = count($node->children());

            $speakerIds = $contentNode->attr('sdi:speaker');
            if (count($speakerIds) == 0) {
                $speakerIds = $node->attr('sdi:speaker');
            }

            // set dialog speakers
            if ($speakerIds) {
                $speakerIds = explode(' ', $speakerIds);
                foreach ($speakerIds as $speakerId) {
                    /** @var $character Character */
                    if (!isset($characters[$speakerId])) {
                        throw new \Exception(sprintf('There is no character defined for speaker "%s"', $speakerId), Response::HTTP_BAD_REQUEST);

                    }
                    $character = $characters[$speakerId];

                    // @todo calculating first tc should not be here, move it somewhere else
                    if (null === $character->getFirstTC()
                        || $this->convertTcTodouble($character->getFirstTC()) > $this->convertTcTodouble($dialog->getTcIn())
                    ) {
                        $character->setFirstTC($dialog->getTcIn());
                    }
                    $character->addDialog($dialog);
                    $dialog->addSpeaker($character);
                }
            }

            $listenerIds = $contentNode->attr('sdi:listener');
            if (count($listenerIds) == 0) {
                $listenerIds = $node->attr('sdi:listener');
            }

            // set dialog listeners
            if ($listenerIds) {
                $listenerIds = explode(' ', $listenerIds);
                foreach ($listenerIds as $listenerId) {
                    if (!isset($characters[$listenerId])) {
                        throw new \Exception(sprintf('There is no character defined for listener "%s"', $listenerId), Response::HTTP_BAD_REQUEST);
                    }

                    $dialog->addListener($characters[$listenerId]);
                }
            }

            if ($dialog->getRole() == 'song') {
                $songCrawler = $this->dialogCrawler->filter(sprintf('sdi|song[id="%s"]', $node->attr('sdi:song')));
                if (0 === count($songCrawler)) {
                    $songName = $node->attr('sdi:song');
                    if ($songName) {
                        throw new \Exception(sprintf('Song (%s) doesn\'t exist for dialog (%s)', $songName, $i), Response::HTTP_BAD_REQUEST);
                    } else {
                        //throw new \Exception(sprintf('Song doesn\'t exist for dialog (%s)', $i), Response::HTTP_BAD_REQUEST);
                    }
                } else {
                    /** @var $character Character */
                    foreach ($dialog->getSpeakers() as $character) {
                        if (!isset($characters[$character->getId()])) {
                            throw new \Exception(sprintf('There is no character defined for speaker "%s" in song dialog', $character->getId()), Response::HTTP_BAD_REQUEST);
                        }

                        $characters[$character->getId()]->setSings(true);
                    }

                    $song = new Song();
                    $song
                        ->setId($node->attr('sdi:song'))
                        ->setName($songCrawler->filter('sdi|name')->text())
                        ->setTranslatedName($this->isFromSource() ? '' : $songCrawler->filter('sdi|translatedName')->text());

                    $agent = $songCrawler->filter('sdi|agent');

                    if (count($agent)) {
                        $song->setLyricist($songCrawler->filter('sdi|agent')->attr('entityId'));
                    }

                    $dialog->setSong($song);

                    $this->songs[] = $song;
                }
            }

            $metadataCrawler = $contentNode->filter('tt|metadata');

            if ($metadataCrawler) {
                $noteCrawler = $metadataCrawler->filter('sdi|note');
                $dialog->setNote(count($noteCrawler) ? $noteCrawler->text() : '');

                $nodeContent = $node->getNode(0);

                foreach ($nodeContent->childNodes as $child) {
                    if ($child->localName == 'metadata') {
                        $nodeContent->removeChild($child);
                        break;
                    }
                }
            }

            $dialog->setNode($node->getNode(0));
            $rawText = '';
            if ($children <= 1) {
                $rawText = $this->getInnerXml($contentNode->getNode(0));
            }

            if (empty($rawText) && $node->text()) {
                if ($children > 1) {
                    foreach ($node->children() as $nodeChildren) {
                        $rawText .= $this->getInnerXml($nodeChildren);

                        if ($nodeChildren->tagName == 'tt:br') {
                            $rawText .= "\n";
                        }
                    }
                } else {
                    $rawText = $node->html();
                }
            }

            $clearedText = $rawText;

            if ($this->clearText) {
                $clearedText = preg_replace('/<tt:br \/>/i', "\n", $clearedText);
                $clearedText = preg_replace('/<tt:br\/>/i', "\n", $clearedText);
                $clearedText = preg_replace('/<tt:br>/i', "\n", $clearedText);
                $clearedText = preg_replace('/<tt:span([^<]*)sdi:role="parenthetical"([^>]*)>([^<]*)<\/tt:span>/i', '$3', $clearedText);

                $clearedText = trim(strip_tags($clearedText));
                $clearedText = preg_replace(array(
                    '/[ ]+/', '/[ ]+\./', '/[ ]+\,/', '/[ ]+\!/', '/[ ]+\?/', '/\(\(/', '/\)\)/', '/&lt;/', '/&gt;/'
                ), array(
                    " ", '.', ',', '!', '?', '(', ')', '<', '>'
                ), $clearedText);
            }

            $clearedText = preg_replace(array('/\t/', '/  /', '/\n/'), array('', '', ''), $clearedText);
            $dialog->setText($clearedText);

            $dialogs[$dialogId] = $dialog;
        });

        $this->characters = $characters;
        $this->dialogs = $dialogs;
    }

    /**
     * @param string $tc time code in format 00:00:00:00
     * @return float
     */
    protected function convertTcToDouble($tc)
    {
        if (trim($tc) != '') {
            $parts = explode(':', $tc);
            $int = ($parts[0] * 60 * 60) + $parts[1] * 60 + $parts[2];
            $frames = $parts[3];
            $double = $int . '.' . $frames;

            return (double)$double;
        }

        return 0.0;
    }

    /**
     * Get inner xml of DomNode
     *
     * @param \DomNode $node
     * @return string
     */
    protected function getInnerXml($node)
    {
        if (!$node) return '';

        $innerXml = '';
        $children = $node->childNodes;
        foreach ($children as $child) {
            $innerXml .= $child->ownerDocument->saveXML($child);
        }

        return $innerXml;
    }
}