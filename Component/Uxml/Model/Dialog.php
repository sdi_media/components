<?php

namespace SDI\ComponentsBundle\Component\Uxml\Model;

class Dialog implements \JsonSerializable
{
    protected $id;
    protected $lineNumber;
    protected $tcIn;
    protected $tcOut;
    protected $duration;
    protected $role;
    protected $note;
    protected $text;
    protected $node;
    protected $song;
    protected $speakers;
    protected $listeners;
    protected $isSource = false;

    public function __construct()
    {
        $this->speakers = array();
        $this->listeners = array();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param int $lineNumber
     * @return $this
     */
    public function setLineNumber($lineNumber)
    {
        $this->lineNumber = $lineNumber;

        return $this;
    }

    /**
     * @return int
     */
    public function getLineNumber()
    {
        return $this->lineNumber;
    }

    public function getTcIn()
    {
        return $this->tcIn;
    }

    public function setTcIn($tcIn)
    {
        $this->tcIn = $tcIn;

        return $this;
    }

    public function getTcOut()
    {
        return $this->tcOut;
    }

    public function setTcOut($tcOut)
    {
        $this->tcOut = $tcOut;

        return $this;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @param Song $song
     * @return $this
     */
    public function setSong($song)
    {
        $this->song = $song;

        return $this;
    }

    /**
     * @return Song
     */
    public function getSong()
    {
        return $this->song;
    }

    /**
     * @return array
     */
    public function getSpeakers()
    {
        return $this->speakers;
    }

    /**
     * @return array
     */
    public function getSpeakersIds()
    {
        $speakerIds = [];

        foreach ($this->speakers as $speaker) {
            $speakerIds[] = $speaker->getId();
        }
        return $speakerIds;
    }

    public function setSpeakers(array $speakers)
    {
        $this->speakers = $speakers;

        return $this;
    }

    public function addSpeaker(Character $character)
    {
        $this->speakers[] = $character;

        return $this;
    }

    public function getListeners()
    {
        return $this->listeners;
    }

    public function addListener(Character $character)
    {
        $this->listeners[] = $character;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNode()
    {
        return $this->node;
    }

    /**
     * @param mixed $node
     */
    public function setNode($node)
    {
        $this->node = $node;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     * @return $this
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSource()
    {
        return $this->isSource;
    }

    /**
     * @param boolean $isSource
     * @return $this
     */
    public function setIsSource($isSource)
    {
        $this->isSource = $isSource;

        return $this;
    }

    public function calculateDuration($frameRate = 30)
    {
        if (empty($frameRate)) {
            $frameRate = 30;
        }

        $this->duration = '';
        $in = preg_split('/[:\.]/', $this->tcIn);
        $out = preg_split('/[:\.]/', $this->tcOut);

        if (count($in) == 4 && count($out) == 4) {
            $framesIn = ((int)$in[0] * 60 * 60 * $frameRate) + ((int)$in[1] * 60 * $frameRate) + ((int)$in[2] * $frameRate) + (int)$in[3];
            $framesOut = ((int)$out[0] * 60 * 60 * $frameRate) + ((int)$out[1] * 60 * $frameRate) + ((int)$out[2] * $frameRate) + (int)$out[3];

            $duration = $framesOut - $framesIn;

            $sec = floor($duration / $frameRate);
            $frames = floor($duration % $frameRate);


            $this->duration = $sec . '.' . ($frames < 10 ? '0' . $frames : $frames);

        }

        return $this->duration;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            $this->getIsSource() ? 's' : 't' => [
                'id' => $this->getId(),
                'in' => $this->getTcIn(),
                'out' => $this->getTcOut(),
                'txt' => $this->getText(),
                'type' => $this->getRole(),
                'note' => $this->getNote(),
                'song' => $this->getSong() ? $this->getSong()->getId() : '',
                's' => $this->getSpeakersIds()
            ],
            $this->getIsSource() ? 't' : 's' => []
        ];
    }
}
