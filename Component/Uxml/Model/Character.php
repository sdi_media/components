<?php

namespace SDI\ComponentsBundle\Component\Uxml\Model;

use SDI\ComponentsBundle\Component\Uxml\Model\Character\Actor;
use SDI\ComponentsBundle\Component\Uxml\Model\Character\Stat;

class Character implements \JsonSerializable
{
    /**
     * @var int $id entity id
     */
    protected $id;

    /**
     * @var string $name originalName
     */
    protected $name;

    /**
     * @var string $age age
     */
    protected $age;

    /**
     * @var string $roleType roleType
     */
    protected $roleType;

    /**
     * @var string $type type
     */
    protected $type;

    /**
     * @var string $translatedName translatedName
     */
    protected $translatedName;

    /**
     * @var string $gender entity gender
     */
    protected $gender;

    /**
     * @var Actor $actor info about actor (optional)
     */
    protected $actor;

    /**
     * @var boolean $sings whether actor sings
     */
    protected $sings;

    /**
     * @var string First TC of character
     */
    protected $firstTC;

    /**
     * @var Stat
     */
    protected $stat;

    /**
     * @var Dialog[]
     */
    protected $dialogs = array();

    public function __construct()
    {
        $this->sings = false;
        $this->firstTC = null;
        $this->actor = null;
        $this->stat = new Stat();
        $this->dialogs = array();
    }

    /**
     * @return string get unique character id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id set unique character id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string get character name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name character name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return null|string character first time code
     */
    public function getFirstTC()
    {
        return $this->firstTC;
    }

    /**
     * @param null|string $firstTC character first time code
     * @return $this
     */
    public function setFirstTC($firstTC)
    {
        $this->firstTC = $firstTC;

        return $this;
    }

    /**
     * @param string $translatedName character translated name
     * @return $this
     */
    public function setTranslatedName($translatedName)
    {
        $this->translatedName = $translatedName;

        return $this;
    }

    /**
     * @return string character translated name
     */
    public function getTranslatedName()
    {
        return $this->translatedName;
    }

    /**
     * @param Actor $actor info about actor
     * @return $this
     */
    public function setActor(Actor $actor)
    {
        $this->actor = $actor;

        return $this;
    }

    /**
     * @return Actor corresponding Actor entry
     */
    public function getActor()
    {
        return $this->actor;
    }

    /**
     * @param string $gender gender description
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string gender description
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param boolean $sings whether character sings
     * @return $this
     */
    public function setSings($sings)
    {
        $this->sings = $sings;

        return $this;
    }

    /**
     * @return boolean whether character sings
     */
    public function getSings()
    {
        return $this->sings;
    }

    /**
     * @param Stat $stat character stats
     * @return $this
     */
    public function setStat(Stat $stat)
    {
        $this->stat = $stat;

        return $this;
    }

    /**
     * @return Stat get character stats
     */
    public function getStat()
    {
        return $this->stat;
    }

    /**
     * @param Dialog $dialog
     * @return $this
     */
    public function addDialog(Dialog $dialog)
    {
        $this->dialogs[$dialog->getId()] = $dialog;

        return $this;
    }

    /**
     * @return Dialog[] get character dialogs
     */
    public function getDialogs()
    {
        return $this->dialogs;
    }

    /**
     * @return string
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param string $age
     * @return $this
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoleType()
    {
        return $this->roleType;
    }

    /**
     * @param string $roleType
     * @return $this
     */
    public function setRoleType($roleType)
    {
        $this->roleType = $roleType;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize() {
        return [
            'id' => $this->getId(),
            'age' => $this->getAge(),
            'gender' => $this->getGender(),
            'name' => $this->getName(),
            'translation' => $this->getTranslatedName(),
            'roleType' => $this->getRoleType(),
            'type' => $this->getType(),
            'actor' => $this->getActor()
        ];
    }
}
