<?php

namespace SDI\ComponentsBundle\Component\Uxml\Model\Character;

class Stat
{
    protected $loops;
    protected $words;
    protected $letters;
    protected $lines;

    public function __construct()
    {
        $this->loops = 0;
        $this->words = 0;
        $this->letters = 0;
        $this->lines = 0;
    }

    public function setLetters($letters)
    {
        $this->letters = $letters;
    }

    public function addToLetters($val)
    {
        $this->letters += $val;
    }

    public function getLetters()
    {
        return $this->letters;
    }

    public function setLines($lines)
    {
        $this->lines = $lines;
    }

    public function addToLines($val)
    {
        $this->lines += $val;
    }

    public function getLines($divisorTypeKey, $divisorValue)
    {
        $res = 0;
        if ($divisorTypeKey == 'key-words') {
            $res = $this->getWords() / $divisorValue;
        }

        if ($divisorTypeKey == 'key-alpha-characters') {
            $res = $this->getLetters() / $divisorValue;
        }
        return $res;
    }

    public function setLoops($loops)
    {
        $this->loops = $loops;
    }

    public function addToLoops($val)
    {
        $this->loops += $val;
    }

    public function getLoops()
    {
        return $this->loops;
    }

    public function setWords($words)
    {
        $this->words = $words;
    }

    public function addToWords($val)
    {
        $this->words += $val;
    }

    public function getWords()
    {
        return $this->words;
    }
}