<?php

namespace SDI\ComponentsBundle\Component\Uxml\Model;

class Song implements \JsonSerializable
{
    protected $id;
    protected $name;
    protected $lyricist;
    protected $translatedName;

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $translatedName
     * @return $this
     */
    public function setTranslatedName($translatedName)
    {
        $this->translatedName = $translatedName;

        return $this;
    }

    /**
     * @return string
     */
    public function getTranslatedName()
    {
        return $this->translatedName;
    }

    /**
     * @return mixed
     */
    public function getLyricist()
    {
        return $this->lyricist;
    }

    /**
     * @param mixed $lyricist
     * @return $this
     */
    public function setLyricist($lyricist)
    {
        $this->lyricist = $lyricist;

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'translation' => $this->getTranslatedName(),
            'lyricist' => $this->getLyricist()
        ];
    }
}
