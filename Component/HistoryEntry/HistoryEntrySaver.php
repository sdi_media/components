<?php

namespace SDI\ComponentsBundle\Component\HistoryEntry;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\DBAL\Connection;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use \Psr\Log\LoggerInterface;

class HistoryEntrySaver
{
    /**
     * @var Connection
     */
    private $db;

    /**
     * @var TokenInterface
     */
    private $tokenStorage;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(Registry $doctrine, TokenStorageInterface $tokenStorage, Serializer $serializer, LoggerInterface $logger)
    {
        $this->db = $doctrine->getConnection('log_database');
        $this->tokenStorage = $tokenStorage->getToken();
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    /**
     * @param HistoryEntryDTO[] $entryHistoryCollection
     */
    public function saveEntryHistoryCollection($entryHistoryCollection)
    {
        foreach ($entryHistoryCollection as $historyEntry) {
            $this->saveSingleEntryHistory($historyEntry);
        }
    }

    /**
     * @param HistoryEntryDTO $historyEntry
     */
    private function saveSingleEntryHistory(HistoryEntryDTO $historyEntry)
    {
        $this->fillUserData($historyEntry);
        $this->serializeObject($historyEntry);

        $sql = 'INSERT INTO public.entry_history(
            event_name, user_id, user_email, user_name, object_class, object_id, created_at, message, request_data, serialized_data)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

        try {
            $this->db->executeUpdate($sql, $historyEntry->getQueryParams());
        } catch (\Exception $e) {
            $this->logger->critical('Add history entry failed: ' . $e->getMessage());
            return;
        }
    }

    private function fillUserData(HistoryEntryDTO $historyEntryDTO)
    {
        if ($this->tokenStorage) {
            $historyEntryDTO->setUserData($this->tokenStorage->getUser()->getFullName(), $this->tokenStorage->getUser()->getEmail(), $this->tokenStorage->getUser()->getId());
        } else {
            $historyEntryDTO->setUserData('Console App');
        }
    }

    private function serializeObject(HistoryEntryDTO $historyEntryDTO)
    {
        $sc = new SerializationContext();
        $sc->setGroups(['entry_history'])
            ->enableMaxDepthChecks();

        $historyEntryDTO->setSerializedData($this->serializer->serialize($historyEntryDTO->getObjectToSerialize(), 'json', $sc));
    }
}