<?php

namespace SDI\ComponentsBundle\Component\HistoryEntry;

use Doctrine\Common\Util\ClassUtils;

class HistoryEntryDTO
{
    /** @var int */
    private $id;

    /**  @var string */
    private $eventName;

    /** @var integer */
    private $userId = null;

    /** @var string */
    private $userEmail = null;

    /** @var string */
    private $userName;

    /** @var integer */
    private $objectClass;

    /** @var integer */
    private $objectId;

    /** @var \DateTime */
    private $createdAt;

    /** @var string */
    private $message;

    /** @var string */
    private $requestData;

    /** @var string */
    private $serializedData;

    private $objectToSerialize;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getEventName()
    {
        return $this->eventName;
    }

    public function setEventName($eventName)
    {
        $this->eventName = $eventName;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @return int
     */
    public function getObjectClass()
    {
        return $this->objectClass;
    }

    /**
     * @return int
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getRequestData()
    {
        return $this->requestData;
    }

    public function setRequestData($requestData)
    {
        $this->requestData = $requestData;
        return $this;
    }

    /**
     * @return string
     */
    public function getSerializedData()
    {
        return $this->serializedData;
    }

    public function setSerializedData($serializedData)
    {
        $this->serializedData = $serializedData;
        return $this;
    }

    /**
     * @return array
     */
    public function getQueryParams()
    {
        return [$this->eventName, $this->userId, $this->userEmail, $this->userName, $this->objectClass, $this->objectId, $this->createdAt->format('Y-m-d H:i:s'), $this->message, $this->requestData, $this->serializedData];
    }

    public function getObjectToSerialize()
    {
        return $this->objectToSerialize;
    }

    public function setObjectToSerialize($object)
    {
        $this->objectToSerialize = $object;
        return $this;
    }

    public function setUserData($userName, $userEmail = null, $userId = null)
    {
        $this->userName = $userName;
        $this->userEmail = $userEmail;
        $this->userId = $userId;

        return $this;
    }

    public function setObject($object)
    {
        $this->objectId = $object->getId();
        $this->objectClass = ClassUtils::getClass($object);
        return $this;
    }

    /**
     * @param array $historyEntry
     * @return HistoryEntryDTO
     */
    public function build($historyEntry)
    {
        $self = new self();
        $self->eventName = $historyEntry['event_name'];
        $self->userId = $historyEntry['user_id'];
        $self->userEmail = $historyEntry['user_email'];
        $self->userName = $historyEntry['user_name'];
        $self->objectClass = $historyEntry['object_class'];
        $self->objectId = $historyEntry['object_id'];
        $self->createdAt = new \DateTime($historyEntry['created_at']);
        $self->message = $historyEntry['message'];
        $self->requestData = $self->decodeData($historyEntry['request_data']);
        $self->serializedData = $self->decodeData($historyEntry['serialized_data']);
        $self->id = $historyEntry['id'];
        return $self;
    }

    /**
     * @param string $jsonData
     * @return array|null
     */
    protected function decodeData($jsonData)
    {
        return $jsonData ? json_decode($jsonData, true) : null;
    }
}