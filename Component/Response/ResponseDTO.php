<?php

namespace SDI\ComponentsBundle\Component\Response;

use JMS\Serializer\Annotation as Serializer;
use SDI\Component;

/**
 * @Serializer\ExclusionPolicy("none")
 * @Serializer\XmlRoot("response")
 */
class ResponseDTO extends Component\Response\ResponseDTO
{
}