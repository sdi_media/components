<?php

namespace SDI\ComponentsBundle\Component\Response;

use JMS\Serializer\Annotation as Serializer;
use SDI\Component;

/**
 * @Serializer\XmlRoot("response")
 */
class RestResponse extends Component\Response\RestResponse
{
}