<?php

namespace SDI\ComponentsBundle\Component\TermWeb;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Filesystem\Filesystem;

class Api
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $session;
    /**
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    private $fileSystem;
    /**
     * @var
     */
    private $sessionFilePath;
    /**
     * @var
     */
    private $config;
    /**
     * @var null
     */
    public $sessionId = null;

    public function __construct(Session $session, $termwebConfig, Filesystem $fileSystem)
    {
        $this->session = $session;
        $this->fileSystem = $fileSystem;
        $this->config = $termwebConfig;
    }

    /**
     * @param string $method
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function doRequest($method = '', $params = array())
    {
        if (empty($method)) {
            throw new \Exception('TermWebApi requires a method', 500);
        }

        $this->sessionId = $this->getSession();

        $noLoginResponse = '';
        if (empty($this->sessionId)) {
            $noLoginResponse = $this->login();
        }

        if (empty($this->sessionId)) {
            throw new \Exception($noLoginResponse['response'], 500);
        }

        $xrpcClient = new \Zend\XmlRpc\Client($this->config['url'], $this->getHttpClient());

        if (isset($this->config['prefix'])) {
            if (!preg_match('/^' . $this->config['prefix'] . '/', $method)) {
                $method = $this->config['prefix'] . $method;
            }
        }

        $xrpc = new \Zend\XmlRpc\Request();
        $xrpc->setMethod($method);
        $xrpc->setParams(array_merge(array($this->sessionId), $params));

        try {
            $xrpcClient->doRequest($xrpc);
        } catch (\Exception $e) {
            return array('status' => 'ERROR', 'response' => $method . print_r($xrpc, true));
        }

        $xrpcResponse = $xrpcClient->getLastResponse();

        //DEBUG REUEST
        if ($method == $this->config['prefix'] . 'update') {
            //print_r($xrpc);
        }

        if (!$xrpcResponse->isFault()) {
            return array('status' => 'OK', 'response' => $xrpcResponse->getReturnValue());
        } else {
            throw new \Exception($xrpcResponse->getFault()->getMessage(), $xrpcResponse->getFault()->getCode());
        }

        return array('status' => 'ERROR', 'response' => $xrpcResponse->getFault()->getMessage());
    }

    /**
     * @param string $method
     * @return array
     */
    public function login($method = 'termwebapi2.initSession')
    {
        $this->sessionId = $this->getSession();

        if (!empty($this->sessionId)) {
            $this->logout();
        }

        $xrpcClient = new \Zend\XmlRpc\Client($this->config['url'], $this->getHttpClient());

        $xrpc = new \Zend\XmlRpc\Request();
        $xrpc->setMethod($method);
        $xrpc->setParams(array(
            $this->config['client'],
            $this->config['login'],
            $this->config['password']
        ));

        $xrpcClient->doRequest($xrpc);
        $xrpcResponse = $xrpcClient->getLastResponse();

        if (!$xrpcResponse->isFault()) {
            $this->sessionId = $xrpcResponse->getReturnValue();
            $this->setSession($this->sessionId);
            return array('status' => 'OK', 'response' => $xrpcResponse);
        }

        $this->setSession('');

        return array('status' => 'ERROR', 'response' => $xrpcResponse->getFault()->getMessage());
    }

    /**
     * @param string $method
     * @return array
     */
    public function logout($method = 'termwebapi2.close')
    {
        $this->sessionId = $this->getSession();

        if (!empty($this->sessionId)) {
            $xrpcClient = new \Zend\XmlRpc\Client($this->config['url'], $this->getHttpClient());

            $xrpc = new \Zend\XmlRpc\Request();
            $xrpc->setMethod($method);
            $xrpc->setParams(array($this->sessionId));

            $xrpcClient->doRequest($xrpc);
            $xrpcResponse = $xrpcClient->getLastResponse();

            if (!$xrpcResponse->isFault()) {
                $this->setSession('');
                return array('status' => 'OK', 'response' => $xrpcResponse);
            }

            return array('status' => 'ERROR', 'response' => $xrpcResponse->getFault()->getMessage());
        }
    }

    /**
     * @param array $config
     * @return bool
     */
    public function setConfig(array $config)
    {
        $this->config = $config;
		
		if (isset($this->config['session_store']) && $this->config['session_store'] == 'file') {
			$this->sessionFilePath = $path = implode(DIRECTORY_SEPARATOR, array('files', 'tit', 'termWeb', $this->config['client']));
		}
			
        return true;
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    public function updateConfig($key, $value)
    {
        $this->config[$key] = $value;
        return true;
    }

    /**
     * @return null|string
     */
    public function getSession()
    {
        if ($this->config['session_store'] == 'session') {
            $this->sessionId = $this->session->get('termweb.session.key');
        } else if ($this->config['session_store'] == 'file') {
            $this->sessionId = '';
            if ($this->fileSystem->exists($this->sessionFilePath)) {
                $this->sessionId = @file_get_contents($this->sessionFilePath);
            }
        }
        return $this->sessionId;
    }

    /**
     *
     * @param type $sessionId
     */
    public function setSession($sessionId)
    {
        if ($this->config['session_store'] == 'session') {
            $this->session->set('termweb.session.key', $sessionId);
        } else if ($this->config['session_store'] == 'file') {
            $this->fileSystem->dumpFile($this->sessionFilePath, $sessionId);
        }
        $this->sessionId = $sessionId;
    }

    /**
     * @return \Zend\Http\Client
     */
    public function getHttpClient()
    {

        $clientConfig = array(
            'adapter' => 'Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(
                CURLOPT_FOLLOWLOCATION => TRUE,
                CURLOPT_SSL_VERIFYPEER => FALSE
            ),
        );

        return new \Zend\Http\Client($this->config['url'], $clientConfig);
    }
}