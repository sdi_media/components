<?php

namespace SDI\ComponentsBundle\Component\TermWeb;

use SDI\ComponentsBundle\Component\TermWeb\TermWeb;

class TBX
{
    /**
     *
     * @var type
     */
    private $xml;

    /**
     *
     * @var type
     */
    private $termwebService;

    /**
     * @param TermWeb $termwebService
     */
    public function __construct(TermWeb $termwebService)
    {
        $this->xml = new \SimpleXMLElement($this->doctype() . '<martif/>');
        $this->termwebService = $termwebService;
    }

    /**
     * @param type $client
     * @param type $conceptData
     * @return type
     */
    public function create($client, $conceptData)
    {
        $this->header($client);
        $this->body($conceptData);

        return str_replace(array('lang=', '&lt;', '&gt;'), array('xml:lang=', '<', '>'), $this->xml->asXML());
    }

    /**
     * @param type $conceptData
     */
    public function body($conceptData)
    {
        $text = $this->xml->addChild('text');
        $body = $text->addChild('body');

        if (is_array($conceptData)) {
            foreach ($conceptData as $concept) {
                if (!empty($concept)) {
                    $this->createConcept($body, $concept);
                }
            }
        }
    }

    /**
     * @param \SimpleXMLElement $body
     * @param type $concept
     */
    public function createConcept($body, $concept)
    {
        $termEntry = $body->addChild('termEntry');
        $termEntry->addAttribute('id', $concept['conceptID']);

        $admin = $termEntry->addChild('admin', $this->escape($concept['sectionName']));
        $admin->addAttribute('type', 'termbaseSection');

        $domains = array();
        foreach ($concept['domainIDs'] as $domainID) {
            $domains[] = $this->createDomainPath($domainID);
        }

        $this->createDomainNode($termEntry, $domains, $concept['conceptID']);

        foreach ($concept['fields'] as $field => $value) {
            $descrip = $termEntry->addChild('descrip', $this->escape($value));
            $descrip->addAttribute('id', '');
            $descrip->addAttribute('type', $field);
        }

        $this->createLangSet($termEntry, $concept);
    }

    /**
     * @param \SimpleXMLElement $termEntry
     * @param type $concept
     */
    public function createLangSet($termEntry, $concept)
    {
        foreach ($concept['terms'] as $term) {
            $langSet = $termEntry->addChild('langSet');
            $langSet->addAttribute('lang', $term['language']);

            $tig = $langSet->addChild('tig');
            $tig->addAttribute('id', (isset($term['termID']) ? $term['termID'] : ""));

            $tig->addChild('term', $this->escape($term['name']));

            foreach ($term['fields'] as $field => $value) {
                $descrip = $tig->addChild('descrip', $this->escape($value));
                $descrip->addAttribute('id', '');
                $descrip->addAttribute('type', $field);
            }
        }
    }

    /**
     * @param type $client
     * @param type|string $createdBy
     * @param type|string $version
     * @param type|string $system
     */
    public function header($client, $createdBy = 'Created by SDI', $version = 'Version 3.8', $system = 'SYSTEM "TBXDCSv05b.xml"')
    {
        $martifHeader = $this->xml->addChild('martifHeader');
        $martifHeader->addAttribute('type', 'TBX');
        $martifHeader->addAttribute('lang', 'en');

        $martifHeader->addChild('fileDesc')->addChild('titleStmt')->addChild('title', $client);

        $sourceDesc = $martifHeader->addChild('sourceDesc');
        $sourceDesc->addChild('p', $createdBy);
        $sourceDesc->addChild('p', $version);
        $system = $sourceDesc->addChild('p', $system);
        $system->addAttribute('type', 'DCSName');
    }

    /**
     * @param type $domainID
     * @return type
     */
    public function createDomainPath($domainID)
    {
        $currentDomain = $this->termwebService->getObject('domain', (string)$domainID);
        $domainPath = array();
        $domainPath[] = $currentDomain['name'];

        while ($currentDomain['parent'] != '0') {
            $currentDomain = $this->termwebService->getObject('domain', $currentDomain['parent']);
            $domainPath[] = $currentDomain['name'];
        }

        return $domainPath;
    }

    /**
     * @param type $termEntry
     * @param type $domains
     * @param type $conceptID
     */
    public function createDomainNode($termEntry, $domains, $conceptID)
    {
        foreach ($domains as $domainPathKey => $path) {
            if (count($path) == 1) {
                $descrip = $termEntry->addChild('descrip', end($path));
                $descrip->addAttribute('id', '');
                $descrip->addAttribute('type', 'subjectField');
            } else {
                $descripGrp = $termEntry->addChild('descripGrp');
                foreach ($path as $domainKey => $domain) {
                    $descripID = $conceptID . '-d' . $domainPathKey . '-' . ($domainKey + 1);
                    $type = 'subjectField';
                    $descripTxt = $domain;
                    $name = 'descrip';
                    $id = 'id';

                    if (count($path) - 1 == $domainKey) {
                        $type = 'parent';
                        $name = 'ref';
                        $descripTxt = '';
                        $id = 'target';
                    }

                    $descrip = $descripGrp->addChild($name, $descripTxt);
                    $descrip->addAttribute($id, $descripID);
                    $descrip->addAttribute('type', $type);
                }

                $descrip = $termEntry->addChild('descrip', $domain);
                $descrip->addAttribute('id', $descripID);
                $descrip->addAttribute('type', 'subjectField');
            }
        }
    }

    /**
     * @return string
     */
    public function doctype()
    {
        $doctype = "<?xml version='1.0' encoding='UTF-8' standalone='no'?>\r\n";
        $doctype .= '<!DOCTYPE martif SYSTEM "./TBXcdv04.dtd">';

        return $doctype;
    }

    /**
     * @param type $string
     * @return type
     */
    public function escape($string)
    {
        return '<![CDATA[' . str_replace(
            array("&"), array("&amp;"), $string
        ) . ']]>';
    }
}