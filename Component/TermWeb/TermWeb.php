<?php

namespace SDI\ComponentsBundle\Component\TermWeb;

use SDI\ComponentsBundle\Component\TermWeb\Api;

class TermWeb
{
    /**
     * @var Api
     */
    private $api;

    public $existingConepts = array();

    private $statusLabels = array(
        'not_requested' => 'Not Requested'
    );
    private $createUpdateLog = array();

    /**
     * @param Api $api
     */
    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    /**
     *
     * @return type
     */
    public function login()
    {
        return $this->api->login();
    }

    public function logout()
    {
        return $this->api->logout();
    }

    /**
     * Find object and/or select it
     *
     * @param type $objectType
     * @param type $searchString
     * @param bool|type $setSelected
     * @param type|string $searchField
     * @return bool
     * @throws \Exception
     */
    public function findObjectByName($objectType = null, $searchString = null, $setSelected = false, $searchField = 'name')
    {
        $objects = $this->api->doRequest('getAll', array($objectType));

        if ($objects['status'] != 'OK') {
            $this->api->logout();
            throw new \Exception($objects['response'], 500);
        }

        if (empty($searchString)) {
            return $objects['response'];
        }

        $response = array();
        foreach ($objects['response'] as $object) {
            if ($searchString == $object[$searchField]) {
                $response = $object;
                break;
            }
        }

        //Select object in termweb
        if ($setSelected && isset($response['id'])) {
            $this->api->doRequest('setSelected', array($objectType, (string)$response['id']));
        }

        return $response;
    }

    /**
     * @param $objectData
     * @return
     * @throws \Exception
     * @internal param type $objectType
     * @internal param type $objectId
     */
    public function createObjectInTermWeb($objectData)
    {
        $this->setCreateUpdateLog($objectData);
        $object = $this->api->doRequest('create', array($objectData));
        if ($object['status'] != 'OK') {
            $this->api->logout();
            throw new \Exception($object['response'], 500);
        }

        return $object['response'];
    }

    /**
     * @param $objectData
     * @return
     * @throws \Exception
     * @internal param type $objectType
     * @internal param type $objectId
     */
    public function updateObjectInTermWeb($objectData)
    {
        $this->setCreateUpdateLog($objectData);
        $object = $this->api->doRequest('update', array($objectData));
        if ($object['status'] != 'OK') {
            $this->api->logout();
            throw new \Exception($object['response'], 500);
        }

        return $object['response'];
    }

    /**
     * @param type $objectType
     * @param type $objectId
     * @return bool
     */
    public function getObject($objectType, $objectId)
    {
        $object = $this->api->doRequest('getObject', array($objectType, $objectId));
        if ($object['status'] != 'OK') {
            return false;
        }

        return $object['response'];
    }

    /**
     * @param $objectType
     * @param $objectName
     * @return mixed
     * @throws \Exception
     */
    public function getObjectByName($objectType, $objectName)
    {
        $object = $this->api->doRequest('getObject', array($objectType, $objectName));
        if ($object['status'] != 'OK') {
            $this->api->logout();
            throw new \Exception($object['response'], 500);
        }

        return $object['response'];
    }

    public function searchTerm($name)
    {
        if (empty($name)) return null;

        $result = $this->api->doRequest('getIndexTerms', array($name));

        foreach ($result['response'] as $item) {
            if ($item['term'] == $name) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @param $search
     * @param $language
     * @return array
     */
    public function searchTermWithLanguage($search, $language)
    {
        return $this->api->doRequest('getIndexTerms', array($search, $language));
    }

    /**
     * @param $search
     * @param $language
     * @return array
     */
    public function searchTermEntriesWithLanguage($search, $language)
    {
        return $this->api->doRequest('getTermEntries', array($search, $language));
    }

    /**
     * @param null $section
     * @param string $filter
     * @param null $returnKey
     * @param bool $getConceptEntry
     * @param string $sourceLanguage
     * @throws \Exception
     */
    public function getConceptEntries($section = null, $filter = '', $returnKey = null, $getConceptEntry = true, $sourceLanguage = 'eng')
    {
        //Set filter and section - clear after previous search
        $this->findObjectByName('filter', $filter, true);
        $this->findObjectByName('section', $section, true);

        if (empty($section)) {
            $this->api->doRequest('setAllSelected', array('section'));
        }

        $terms = $this->api->doRequest('getIndexTerms', array(''));
        if (empty($returnKey)) {
            $returnKey = $section;
        }

        $this->existingConepts[$returnKey] = array();

        if ($getConceptEntry) {
            foreach ($terms['response'] as $term) {
                $entry = $this->api->doRequest('getConceptEntry', array((string)$term['conceptEntryID']));
                $this->existingConepts[$returnKey][] = $entry['response'];
            }
        } else {
            $this->existingConepts[$returnKey] = $terms['response'];
        }
    }

    /**
     * @param $conceptEntryID
     * @return null
     * @throws \Exception
     */
    public function getConceptEntry($conceptEntryID)
    {
        $result = $this->api->doRequest('getConceptEntry', array((string)$conceptEntryID));

        if (isset($result['response']) && $result['status'] == 'OK') {
            return $result['response'];
        } else {
            throw new \Exception($result['response']);
        }

        return null;
    }

    /**
     * @param $conceptEntry
     * @param string $language
     * @return null
     */
    public function getTargetLanguageTerm($conceptEntry, $language = 'eng')
    {
        foreach ($conceptEntry['terms'] as $term) {
            if ($term['language'] == $language) {
                return $term;
            } else {
                return null;
            }
        }
    }

    /**
     * @param string $isoCode3
     * @return array
     * @throws \Exception
     */
    public function setSourceLanguage($isoCode3 = 'eng')
    {
        //Get available languages
        $languages = $this->findObjectByName('language');

        if (is_array($languages) && count($languages) > 0) {
            foreach ($languages as $lang) {
                if ($isoCode3 == $lang['isoCode3']) {
                    return $this->api->doRequest('setSourceLanguage', array($lang['isoCode3']));
                }
            }
        }

        throw new \Exception(sprintf('Language %s not exists', $isoCode3));
    }

    /**
     * @param $objectType
     * @return array
     * @throws \Exception
     */
    public function getSelected($objectType)
    {
        return $this->api->doRequest('getSelected', array($objectType));
    }

    /**
     * @param $objectType
     * @param $objectId
     * @throws \Exception
     */
    public function setSelected($objectType, $objectId)
    {
        $this->api->doRequest('setSelected', array($objectType, $objectId));
    }

    /**
     * @param array $config
     * @return bool
     */
    public function setApiConfig(array $config)
    {
        $this->api->setConfig($config);

        return true;
    }

    /**
     *
     * @param array $config
     * @return bool
     */
    public function setLoginConfig(array $config)
    {
        foreach($config as $key => $value) {
            $this->api->updateConfig($key, $value);
        }

        return true;
    }

    /**
     * @return Api
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     * @param type $string
     * @return mixed
     */
    public function countWords($string)
    {
        return str_word_count($string, 0, preg_replace('/[0-9a-zA-Z.\-:,\s]+/', '', $string));
    }

    /**
     * @param type|string $dictionaryID
     * @param type|string $sectionName
     * @param null $sectionID
     * @param null $conceptID
     * @param array|type $domains
     * @param array|type $fields
     * @param array|type $terms
     * @return null
     */
    public function createConceptEntry($dictionaryID = '', $sectionName = '', $sectionID = null, $conceptID = null, $domains = array(), $fields = array(), $terms = array())
    {
        if (empty($dictionaryID) || empty($sectionName)) {
            return null;
        }

        $concept_entry = array(
            '__objtype' => 'ConceptEntry',
            'sectionName' => $sectionName,
            'conceptID' => ($conceptID ? $conceptID : uniqid()),
            'dictionaryID' => $dictionaryID,
            'domainIDs' => $domains,
            'fields' => $fields,
            'terms' => $terms
        );

        if ($sectionID) {
            $concept_entry['sectionID'] = $sectionID;
        }


        if (isset($conceptID) && !empty($conceptID)) {
            $concept_entry['conceptID'] = $conceptID;
        }

        return $concept_entry;
    }

    /**
     * @param type|string $dictionaryID
     * @param type|string $sectionName
     * @return null
     * @throws \Exception
     */
    public function createSection($dictionaryID = '', $sectionName = '')
    {
        if (empty($dictionaryID) || empty($sectionName)) {
            return null;
        }

        $section_entry = array(
            '__objtype' => 'Section',
            'name' => $sectionName,
            'dictionaryID' => $dictionaryID
        );

        $section_entry = $this->createObjectInTermWeb($section_entry);

        return $section_entry;
    }

    /**
     * @param $originalEntry
     * @param type $newEntry
     * @param string $compareKey
     * @param bool $forceFields
     * @return type
     * @internal param type $concept
     */
    public function updateEntry($originalEntry, $newEntry, $compareKey = 'language', $forceFields = true)
    {
        $conceptEntryID = null;

        if (isset($originalEntry['conceptEntryID'])) {
            $conceptEntryID = $originalEntry['conceptEntryID'];
        }

        foreach ($newEntry as $newEntryKey => $newEntryValue) {
            if ($newEntryKey == 'terms' && is_array($newEntryValue)) {
                foreach ($newEntryValue as $newTerm) {
                    $termExists = false;
                    $translationUID = null;

                    //If new term contains translation UID - it need to be compared also
                    if (isset($newTerm['fields']) && isset($newTerm['fields']['Translation UID']) && !empty($newTerm['fields']['Translation UID'])) {
                        $translationUID = $newTerm['fields']['Translation UID'];
                    }

                    if (is_array($compareKey)) {
                        $newTermValueToCompare = $this->findValueInEntry($newTerm, $compareKey);
                        if (empty($newTermValueToCompare) && $newTerm['language'] == 'eng') {
                            $newTermValueToCompare = $this->findValueInEntry($newTerm, array('fields', 'CAT UID')) . '_' . $newTerm['language'];
                        }

                        if (isset($originalEntry[$newEntryKey])) {
                            foreach ($originalEntry[$newEntryKey] as $entryTermKey => $originalEntryTerm) {
                                $originalTermValueToCompare = $this->findValueInEntry($originalEntryTerm, $compareKey);

                                if (empty($originalTermValueToCompare) && $originalEntryTerm['language'] == 'eng') {
                                    $originalTermValueToCompare = $this->findValueInEntry($originalEntryTerm, array('fields', 'CAT UID')) . '_' . $originalEntryTerm['language'];
                                }

                                if ($newTermValueToCompare == $originalTermValueToCompare) {
                                    $originalEntry[$newEntryKey][$entryTermKey] = $this->updateEntry($originalEntry[$newEntryKey][$entryTermKey], $newTerm);
                                    if (!empty($conceptEntryID)) {
                                        $originalEntry[$newEntryKey][$entryTermKey]['conceptEntryID'] = $conceptEntryID;
                                    }
                                    $termExists = true;
                                }
                            }
                        }
                    } else {
                        if (isset($originalEntry[$newEntryKey])) {
                            foreach ($originalEntry[$newEntryKey] as $entryTermKey => $originalEntryTerm) {
                                if (isset($newTerm[$compareKey]) && isset($originalEntryTerm[$compareKey]) && $originalEntryTerm[$compareKey] == $newTerm[$compareKey]) {
                                    if ($compareKey == 'language' && !empty($translationUID)) {
                                        if (isset($originalEntryTerm['fields']) && isset($originalEntryTerm['fields']['Translation UID']) && $originalEntryTerm['fields']['Translation UID'] != $translationUID) {
                                            $termExists = true;
                                            continue;
                                        }
                                    } else if ($compareKey == 'language' && isset($originalEntryTerm['fields']['CAT UID']) && !empty($originalEntryTerm['fields']['CAT UID']) && isset($newTerm['fields']['Translation Status']) && $newTerm['fields']['Translation Status'] == $this->statusLabels['not_requested']) {
                                        if (!isset($originalEntryTerm['fields']['Translation Status'])) {
                                            $termExists = true;
                                            continue;
                                        } else if ($originalEntryTerm['fields']['Translation Status'] != $this->statusLabels['not_requested']) {
                                            $termExists = true;
                                            continue;
                                        } else if ($originalEntryTerm['fields']['CAT UID'] == $newTerm['fields']['CAT UID']) {
                                            $termExists = true;
                                            continue;
                                        }
                                    }

                                    if (isset($originalEntry[$newEntryKey][$entryTermKey]['fields']['Translation Status']) && $originalEntry[$newEntryKey][$entryTermKey]['fields']['Translation Status'] == $this->statusLabels['not_requested']) {
                                        unset($originalEntry[$newEntryKey][$entryTermKey]['fields']['Translation Status']);
                                    }

                                    if (isset($originalEntry[$newEntryKey][$entryTermKey]['toBeDeleted']) && $originalEntry[$newEntryKey][$entryTermKey]['toBeDeleted'] == 'False') {
                                        unset($originalEntry[$newEntryKey][$entryTermKey]['toBeDeleted']);
                                    }

                                    if ($compareKey == 'language' && isset($originalEntryTerm['fields']['CAT UID']) && isset($newTerm['fields']['CAT UID']) && $newTerm['language'] == 'eng') {
                                        if ($newTerm['fields']['CAT UID'] == $originalEntryTerm['fields']['CAT UID']) {
                                            $originalEntry[$newEntryKey][$entryTermKey] = $this->updateEntry($originalEntry[$newEntryKey][$entryTermKey], $newTerm);
                                        }
                                    } else {
                                        if (isset($newTerm['fields']['CAT UID']) && isset($originalEntryTerm['fields']['CAT UID']) && $newTerm['fields']['CAT UID'] != $originalEntryTerm['fields']['CAT UID']) {
                                            $termExists = false;
                                            continue;
                                        } else {
                                            $originalEntry[$newEntryKey][$entryTermKey] = $this->updateEntry($originalEntry[$newEntryKey][$entryTermKey], $newTerm);
                                        }
                                    }

                                    if (!empty($conceptEntryID)) {
                                        $originalEntry[$newEntryKey][$entryTermKey]['conceptEntryID'] = $conceptEntryID;
                                    }
                                    $termExists = true;
                                }
                            }
                        }
                    }
                    if (!$termExists) {
                        $newTerm['termID'] = '';
                        $newTerm['id'] = '';
                        if (!empty($conceptEntryID)) {
                            $newTerm['conceptEntryID'] = $conceptEntryID;
                        }
                        $originalEntry[$newEntryKey][] = $newTerm;
                    }
                }
            } else if ($newEntryKey == 'fields') {
                if ($forceFields) {
                    if (!is_array($newEntryValue)) {
                        $newEntryValue = array();
                    }

                    if (!isset($originalEntry[$newEntryKey]) || !is_array($originalEntry[$newEntryKey])) {
                        $originalEntry[$newEntryKey] = array();
                    }

                    if (isset($originalEntry[$newEntryKey]['1st Appearance: Episode']) && !empty($originalEntry[$newEntryKey]['1st Appearance: Episode']) && isset($newEntryValue['1st Appearance: Episode'])) {
                        unset($newEntryValue['1st Appearance: Episode']);
                    }

                    $originalEntry[$newEntryKey] = array_merge($originalEntry[$newEntryKey], $newEntryValue);
                } else {
                    $originalEntry[$newEntryKey] = $newEntryValue;
                }
            } else {
                if ($newEntryKey == 'conceptEntryID') {
                    if (!isset($originalEntry[$newEntryKey]) || empty($originalEntry[$newEntryKey])) {
                        $originalEntry[$newEntryKey] = $newEntryValue;
                    }
                }
                if ($newEntryKey == 'id' || $newEntryKey == 'termID') {
                    if (empty($originalEntry[$newEntryKey]) || !isset($originalEntry[$newEntryKey])) {
                        $originalEntry[$newEntryKey] = $newEntryValue;
                    }
                } else {
                    $originalEntry[$newEntryKey] = $newEntryValue;

                    if ($newEntryValue == '') {
                        unset($originalEntry[$newEntryKey]);
                    }
                }
            }
        }

        return $originalEntry;
    }

    /**
     * @param $entry
     * @param array $arrayOfKeys
     * @return null
     */
    public function findValueInEntry($entry, $arrayOfKeys = array())
    {
        if (empty($arrayOfKeys)) return null;

        foreach ($arrayOfKeys as $key) {
            if (isset($entry[$key])) {
                $entry = $entry[$key];
            } else {
                return null;
            }
        }

        return $entry;
    }

    /**
     * @param $entryProperties
     * @return type
     * @internal param type $name
     * @internal param type $language
     * @internal param type $fields
     */
    public function createTermEntry($entryProperties)
    {
        $term_entry = array(
            '__objtype' => 'TermEntry',
            'name' => $entryProperties['name'],
            'language' => ((isset($entryProperties['language']) && !empty($entryProperties['language'])) ? $entryProperties['language'] : 'eng'),
            'fields' => (isset($entryProperties['fields']) ? $entryProperties['fields'] : array()),
        );

        if (isset($entryProperties['termID'])) {
            $term_entry['termID'] = $entryProperties['termID'];
        }

        if (isset($entryProperties['dictionaryID'])) {
            $term_entry['dictionaryID'] = $entryProperties['dictionaryID'];
        }

        if (isset($entryProperties['toBeDeleted'])) {
            $term_entry['toBeDeleted'] = $entryProperties['toBeDeleted'];
        }

        return $term_entry;
    }

    /**s
     * @param type $termEntry
     * @param type $entryProperties
     * @return type
     */
    public function createUpdateTermEntry($termEntry, $entryProperties)
    {
        if ($termEntry) {
            unset($entryProperties['language']);
            $termEntry = $this->updateEntry($termEntry, $entryProperties);

            return $termEntry;
        } else {
            return $this->createTermEntry($entryProperties);
        }
    }

    /**
     * @param type $date
     * @return type
     */
    public function formatDate($date)
    {
        return preg_replace('/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)(\d\d)/', '$1-20$2', $date);
    }

    /**s
     * @param type $date
     * @return type
     */
    public function formatDateHighlight($date)
    {
        $date = explode('-', $date);
        $date[1] = date('y', strtotime($date[1]));
        return implode(' ', $date);
    }

    public function createNotRequestedTerms($langMap, $dictionaryId, $catUID = null)
    {
        $terms = [];

        if (empty($catUID)) {
            return $terms;
        }

        foreach ($langMap as $lang) {
            $name = '__Not Requested_' . $catUID;
            $terms[] = $this->createTermEntry(
                array(
                    'name' => $name,
                    'dictionaryID' => $dictionaryId,
                    'toBeDeleted' => 'False',
                    'language' => $lang['isoCode3'],
                    'fields' => array(
                        'Translation Status' => $this->statusLabels['not_requested'], 'CAT UID' => $catUID
                    )
                )
            );
        }

        return $terms;
    }

    /**
     * @param $logItem
     */
    private function setCreateUpdateLog($logItem)
    {
        $this->createUpdateLog[] = $logItem;
    }

    /**
     * @return array
     */
    public function getCreateUpdateLog()
    {
        return $this->createUpdateLog;
    }
}