<?php

namespace SDI\ComponentsBundle\Component\GridEdit;

use Doctrine\ORM\EntityManager;
use SDI\ComponentsBundle\Component\Response\Serialized;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;

class Inline
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var Serialized
     */
    private $serializedResponse;

    /**
     * Inline constructor.
     * @param EntityManager $entityManager
     * @param FormFactory $formFactory
     * @param Serialized $serializedResponse
     */
    public function __construct(EntityManager $entityManager, FormFactory $formFactory, Serialized $serializedResponse)
    {
        $this->em = $entityManager;
        $this->formFactory = $formFactory;
        $this->serializedResponse = $serializedResponse;
    }

    /**
     * Validate submitted entries.
     *
     * @param array $entries
     * @param string $entityClass entity FQN or Doctrine class name
     * @param AbstractType|string $formType form type FQN or form type instance
     * @param array $formOptions form options passed to form type constructor
     * @param \Closure|null $successClosure
     * @return array
     */
    public function batchProcess($entries, $entityClass, $formType, $formOptions = [], $successClosure = null)
    {
        $errors = array();

        foreach ($entries as $entryData) {
            $form = $this->createForm($entryData, $entityClass, $formType, $formOptions);
            $entity = $form->getData();

            if ($form->isValid()) {
                $this->em->persist($entity);

                if (is_callable($successClosure)) {
                    $successClosure($entity);
                }
            } else {
                $errors[] = $this->getFormErrors($form);
            }
        }

        return $errors;
    }

    /**
     * @param array $data submitted data
     * @param string $entityClass entity FQN or Doctrine class name
     * @param AbstractType|string $formType form type FQN or form type instance
     * @param array $formOptions form options passed to form type constructor
     * @param \Closure|null $successClosure
     * @return Response
     */
    public function process($data, $entityClass, $formType, $formOptions = [], $successClosure = null)
    {
        $form = $this->createForm($data, $entityClass, $formType, $formOptions);
        $entity = $form->getData();

        if ($form->isValid()) {
            $this->em->persist($entity);

            $this->em->flush();
            $this->em->refresh($entity);

            if (is_callable($successClosure)) {
                $successClosure($entity);
            }

            return $this->serializedResponse->response(Serialized::generateSuccess());
        }

        $errors = $this->getFormErrors($form);
        return $this->serializedResponse->response(Serialized::generateError('Form error', $errors), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param array $data
     * @param string $entityClass entity FQN or Doctrine class name
     * @param AbstractType|string $formType form type FQN or form type instance
     * @param array $formOptions
     * @return FormInterface
     */
    protected function createForm($data, $entityClass, $formType, $formOptions)
    {
        if (isset($data['id']) && !empty($data['id'])) {
            $entity = $this->em->getRepository($entityClass)->find($data['id']);
            unset($data['id']);
        } else {
            $className = $this->em->getRepository($entityClass)->getClassName();
            $entity = new $className();
        }

        $submitData = array();
        foreach ($data as $property => $val) {
            if (is_array($val)) {
                if (true === array_key_exists('id', $val)) {
                    $submitData[$property] = $val['id'];
                    continue;
                }

                foreach ($val as $valItem) {
                    if (is_array($valItem) && true === array_key_exists('id', $valItem)) {
                        $submitData[$property][] = $valItem['id'];
                        continue;
                    }
                }
            } else if (in_array(strtolower($val), array('true', 'false', '1', '0', 'yes', 'no'))) {
                $submitData[$property] = in_array($val, array('true', '1', 'yes'));
            } else {
                $submitData[$property] = $val;
            }
        }

        $form = $this->formFactory->create($formType, $entity, $formOptions);
        $form->submit($submitData);

        return $form;
    }

    /**
     * Convert form errors to array
     *
     * @param FormInterface $form
     * @return array
     */
    protected function getFormErrors($form)
    {
        $errors = array();
        if ($form instanceof Form) {
            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }

            foreach ($form->all() as $key => $child) {
                $childErrors = $this->getFormErrors($child);
                if ($childErrors) {
                    $errors[$key] = $childErrors;
                }
            }
        }

        return $errors;
    }
}