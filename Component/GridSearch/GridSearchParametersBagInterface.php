<?php

namespace SDI\ComponentsBundle\Component\GridSearch;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

interface GridSearchParametersBagInterface
{
    /**
     * @return integer
     */
    public function getLimit();

    /**
     * @return integer
     */
    public function getOffset();

    /**
     * @return string
     */
    public function getSortDir();

    /**
     * @return string
     */
    public function getSortField();

    /**
     * @return array
     */
    public function getFilters();

    /**
     * @return string
     */
    public function getFiltersLogic();

    /**
     * @return mixed
     */
    public function getSort();

    /**
     * @return array
     */
    public function toArray();
}
