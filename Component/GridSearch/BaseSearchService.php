<?php

namespace SDI\ComponentsBundle\Component\GridSearch;

use Doctrine\ORM\QueryBuilder;

class BaseSearchService
{
    /**
     * @var QueryBuilder
     */
    protected $qb;

    /**
     * @param QueryBuilder $qb - Backword compatibility - should be null;
     * @param GridSearchParametersBagInterface $gridSearchParametersBag
     * @param array $mapping
     * @return $this
     */
    protected function applyFilters(QueryBuilder $qb, GridSearchParametersBagInterface $gridSearchParametersBag, array $mapping)
    {
        //Backward compatibility
        if ($qb) {
            $this->qb = $qb;
        }

        $filters = $gridSearchParametersBag->getFilters();

        if (0 === count($filters)) {
            return $this;
        }

        $filtersLogic = $gridSearchParametersBag->getFiltersLogic();
        $this->createFiltersQuery($filters, $mapping, $filtersLogic);

        return $this;
    }

    /**
     * @param QueryBuilder $qb - Backward compatibility - should be null;
     * @param GridSearchParametersBagInterface $gridSearchParametersBag
     * @param array $mapping
     * @return $this
     */
    protected function applyOrderBy(QueryBuilder $qb, GridSearchParametersBagInterface $gridSearchParametersBag, array $mapping)
    {
        if ($sort = $gridSearchParametersBag->getSort()) {
            $index = 0;
            foreach ($sort as $sortItem) {
                $column = $mapping[$sortItem['field']];
                $dir = $sortItem['dir'];

                if ($column instanceof MappingColumn) {
                    $column = $column->getColumn();
                }

                $orderByMethod = 'addOrderBy';

                if ($index == 0) {
                    $orderByMethod = 'orderBy';
                }

                if (is_array($column)) {
                    foreach ($column as $col) {
                        $this->qb->$orderByMethod($col, strtoupper($dir));
                    }
                } else {
                    $this->qb->$orderByMethod($column, strtoupper($dir));
                }
                $index++;
            }
        } else if ($sortField = $gridSearchParametersBag->getSortField()) {
            $column = $mapping[$sortField];

            if ($column instanceof MappingColumn) {
                $column = $column->getColumn();
            }

            if (is_array($column)) {
                foreach ($column as $col) {
                    $this->qb->addOrderBy($col, strtoupper($gridSearchParametersBag->getSortDir()));
                }
            } else {
                $this->qb->orderBy($column, strtoupper($gridSearchParametersBag->getSortDir()));
            }
        }

        return $this;
    }

    /**
     * @param GridSearchParametersBagInterface $gridSearchParametersBag
     */
    protected function applyPagination(GridSearchParametersBagInterface $gridSearchParametersBag)
    {
        $this->qb->setFirstResult($gridSearchParametersBag->getOffset())
            ->setMaxResults($gridSearchParametersBag->getLimit());
    }

    /**
     * Reset DQL orderBy and clears pagination parameters
     */
    protected function resetPaginationAndOrderBy()
    {
        $this->qb->resetDQLPart('orderBy');

        $this->qb
            ->setFirstResult(null)
            ->setMaxResults(null);
    }

    /**
     * @param array $filters
     * @param array $mapping
     * @param string $filtersLogic
     * @return $this
     * @throws \Exception
     */
    public function createFiltersQuery(array $filters, array $mapping, $filtersLogic = 'and', $recursive = false)
    {
        $expressions = [];
        foreach ($filters as $filter) {
            if (isset($filter['filters'])) {
                $expressions[] = $this->createFiltersQuery($filter['filters'], $mapping, (isset($filter['logic']) ? $filter['logic'] : 'and'), true);
            } else {
                $column = $mapping[$filter['field']];

                $value = $filter['value'];

                if (strpos($value, 'GMT') !== false) { // value can be a date
                    // Thu Nov 15 2012 00:00:00 GMT-0700 (Mountain Standard Time)
                    $date = \DateTime::createFromFormat('D M d Y H:i:s e+', $value);

                    if ($date instanceof \DateTime) {
                        $value = $date;
                    }
                }

                if ($value === 'true' || $value === 'false') {
                    $value = 'true' === $value;
                }

                if ($column instanceof MappingColumn) {
                    if (is_array($column->getColumn())) {
                        if ($column->getIsConcatenation()) {
                            $subExpressions = array();
                            foreach ($column->getColumn() as $singleColumn) {
                                $subExpressions[] = $this->applyColumnExpression('concat', $singleColumn, $column->getConcatenationSeparator());
                            }

                            $concatenation = reset($subExpressions);
                            foreach ($subExpressions as $index => $subConcatenation) {
                                if ($index > 0) {
                                    $concatenation = $this->applyColumnExpression('concat', $concatenation, $subConcatenation);
                                }
                            }

                            $concatenation = $this->applyColumnExpression($filter['operator'], $concatenation, $value);

                            $expressions[] = new Expression($concatenation, $column->getIsVirtual());
                        } else {
                            $subExpressions = array();
                            foreach ($column->getColumn() as $singleColumn) {
                                $subExpressions[] = $this->applyColumnExpression($filter['operator'], $singleColumn, $value);
                            }

                            $expressions[] = new Expression($this->qb->expr()->orX(
                                $subExpressions[0], $subExpressions[1]
                            ), $column->getIsVirtual());
                        }
                    } else {
                        $expressions[] = new Expression($this->applyColumnExpression($filter['operator'], $column->getColumn(), $value), $column->getIsVirtual());
                    }
                } else {
                    //For mappings with simple array
                    if (is_array($column)) {
                        $subExpressions = array();
                        foreach ($column as $singleColumn) {
                            $subExpressions[] = $this->applyColumnExpression($filter['operator'], $singleColumn, $value);
                        }
                        $expressions[] = new Expression($this->qb->expr()->orX(
                            $subExpressions[0], $subExpressions[1]
                        ));
                    } else {
                        $expressions[] = new Expression($this->applyColumnExpression($filter['operator'], $column, $value));
                    }
                }
            }
        }

        if ($recursive) {
            return [
                'logic' => $filtersLogic,
                'expressions' => $expressions,
            ];
        }

        if ('and' === $filtersLogic) {
            /** @var $expression Expression */
            foreach ($expressions as $expression) {
                if (is_array($expression)) {
                    $this->qb->andWhere($this->applyNestedExpressions($expression));
                } else if ($expression->getIsVirtual()) {
                    $this->qb->andHaving($expression->getExpr());
                } else {
                    $this->qb->andWhere($expression->getExpr());
                }
            }
        } elseif ('or' === $filtersLogic) {
            $orX = $this->qb->expr()->orX();

            /** @var $expression Expression */
            foreach ($expressions as $expression) {
                if (is_array($expression)) {
                    $orX->add($this->applyNestedExpressions($expression));
                } else if ($expression->getIsVirtual()) {
                    $this->qb->andHaving($expression->getExpr());
                } else {
                    $orX->add($expression->getExpr());
                }
            }

            $this->qb->andWhere($orX);
        }

        return $this;
    }

    /**
     * @param $operator
     * @param $column
     * @param string $value
     * @return \Doctrine\ORM\Query\Expr\Comparison
     * @throws \Exception
     */
    private function applyColumnExpression($operator, $column, $value)
    {
        $paramName = preg_replace('/[^A-Za-z0-9 ]/', '', $column) . uniqid(rand(0, 100)) . rand(100, 999);
        switch ($operator) {
            case 'eq':
                if (empty($value)) {
                    $expr = $this->qb->expr()->orX(
                        $this->qb->expr()->eq($column, ':' . $paramName),
                        $this->qb->expr()->isNull($column)
                    );
                } else {
                    $expr = $this->qb->expr()->eq($column, ':' . $paramName);
                }

                $this->qb->setParameter($paramName, $value);

                break;
            case 'neq':
                $expr = $this->qb->expr()->orX(
                    $this->qb->expr()->neq($column, ':' . $paramName),
                    $this->qb->expr()->isNull($column)
                );

                $this->qb->setParameter($paramName, $value);

                break;
            case 'gt':
                $expr = $this->qb->expr()->gt($column, ':' . $paramName);
                $this->qb->setParameter($paramName, $value);
                break;
            case 'lt':
                $expr = $this->qb->expr()->lt($column, ':' . $paramName);
                $this->qb->setParameter($paramName, $value);
                break;
            case 'startswith':
                $expr = $this->qb->expr()->like($column, $this->qb->expr()->literal((string)$value . '%'));
                break;
            case 'contains':
                $expr = $this->qb->expr()->like($column, $this->qb->expr()->literal('%' . (string)$value . '%'));
                break;
            case 'doesnotcontain':
                $expr = $this->qb->expr()->notLike($column, $this->qb->expr()->literal('%' . (string)$value . '%'));
                break;
            case 'endswith':
                $expr = $this->qb->expr()->like($column, $this->qb->expr()->literal('%' . (string)$value));
                break;
            case 'inlist':
                $inValues = preg_split("/(,|;)( )?/", $value);
                $inValues = array_filter($inValues, function ($value) {
                    return $value !== '';
                });
                $expr = $this->qb->expr()->in($column, $inValues);
                break;
            case 'inlistwithspaces':
                $inValues = preg_split("/(,|;| )( )?/", $value);
                $inValues = array_filter($inValues, function ($value) {
                    return $value !== '';
                });
                $expr = $this->qb->expr()->in($column, $inValues);
                break;
            case 'notinlist':
                $inValues = preg_split("/(,|;)( )?/", $value);
                $inValues = array_filter($inValues, function ($value) {
                    return $value !== '';
                });
                $expr = $this->qb->expr()->notIn($column, $inValues);
                break;
            case 'concat':
                if (is_string($value)) {
                    $value = $this->qb->expr()->literal($value);
                }

                $expr = $this->qb->expr()->concat(sprintf('COALESCE(%s, \'\')', $column), $value);
                break;
            case 'isnull':
                $expr = $this->qb->expr()->orX(
                    $this->qb->expr()->isNull($column),
                    $this->qb->expr()->eq($column, $this->qb->expr()->literal(''))
                );
                break;
            case 'isnotnull':
                $expr = $this->qb->expr()->andX(
                    $this->qb->expr()->isNotNull($column),
                    $this->qb->expr()->neq($column, $this->qb->expr()->literal(''))
                );
                break;
            default:
                throw new \Exception(sprintf('Operator "%s" is not supported', $operator));
        }

        return $expr;
    }

    /**
     * @param array $expressions

     * @return \Doctrine\ORM\Query\Expr\Andx|\Doctrine\ORM\Query\Expr\Orx
     */
    private function applyNestedExpressions(array $expressions)
    {
        if ($expressions['logic'] === 'or') {
            $addX = $this->qb->expr()->orX();
        } else {
            $addX = $this->qb->expr()->andX();
        }

        foreach ($expressions['expressions'] as $expression) {
            if (is_array($expression)) {
                $addX->add($this->applyNestedExpressions($expression));
            } else if ($expression->getIsVirtual()) {
                $addX->add($expression->getExpr());
            } else {
                $addX->add($expression->getExpr());
            }
        }

        return $addX;
    }
}