<?php

namespace SDI\ComponentsBundle\Component\GridSearch\Adapter;

use Closure;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use SDI\ComponentsBundle\Component\GridSearch\Expression;
use SDI\ComponentsBundle\Component\GridSearch\GridSearchParametersBag;
use SDI\ComponentsBundle\Component\GridSearch\MappingColumn;

class DoctrineAdapter implements SearchAdapterInterface
{
    /** @var EntityManager $em */
    protected $em;

    /** @var QueryBuilder $qb */
    protected $qb;

    /** @var array $mapping */
    protected $mapping;

    /** @var string */
    protected $alias;

    /** @var GridSearchParametersBag GridSearchParametersBag */
    protected $gridSearchParametersBag;

    /**
     * DoctrineAdapter constructor.
     * @param EntityManager $em
     * @param GridSearchParametersBag $gridSearchParametersBag
     */
    public function __construct(EntityManager $em, GridSearchParametersBag $gridSearchParametersBag)
    {
        $this->em = $em;
        $this->gridSearchParametersBag = $gridSearchParametersBag;
    }

    /**
     * @param $class
     * @param $alias
     * @param Closure|null $defaultFilters
     * @return $this|QueryBuilder
     * @throws \Exception
     */
    public function initQueryBuilder($class, $alias, Closure $defaultFilters = null)
    {
        if (!$this->checkIfEntityExists($class)) {
            throw new \Exception(sprintf('Not found entity %s', $class));
        }

        if ($this->qb instanceof QueryBuilder) {
            return $this->qb;
        }

        $this->alias = $alias;

        $this->qb = $this->em->getRepository($class)->createQueryBuilder($alias);

        // apply default filters
        $this->applyNextFilter($defaultFilters);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setMapping($mapping)
    {
        $this->mapping = $mapping;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getMapping()
    {
        return $this->getMapping();
    }

    /**
     * Check if class exists and it is doctrine entity
     *
     * @param $class
     * @return bool
     */
    public function checkIfEntityExists($class)
    {
        if (!class_exists($class)) {
            return false;
        }

        $this->em->getClassMetadata($class);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $this->applyFilters();
        return $this->qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getGridData()
    {
        $this->applyOrderBy();
        $this->applyPagination();

        return $this->qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        $this->resetPaginationAndOrderBy();
        $this->qb->select(sprintf('%s.id', $this->alias));

        return count($this->qb->getQuery()->getArrayResult());
    }

    public function getDataWithColumnsFromMapping()
    {
        $this->applyFilters();
        $this->qb->select($this->getMappingColumns($this->mapping));

        return $this->qb->getQuery()->getArrayResult();
    }

    /**
     * @param MappingColumn[] $mapping
     * @return string
     */
    protected function getMappingColumns($mapping)
    {
        $columnList = array();
        foreach ($mapping as $name => $column) {
            $columnList[] = sprintf('%s AS %s', $column->getColumn(), $name);
        }
        return implode(',', $columnList);
    }

    /**
     * {@inheritdoc}
     */
    public function applyFilters()
    {
        $filters = $this->gridSearchParametersBag->getFilters();

        if (0 === count($filters)) {
            return $this;
        }

        $filtersLogic = $this->gridSearchParametersBag->getFiltersLogic();
        $this->createFiltersQuery($filters, $filtersLogic);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function applyNextFilter(Closure $filters)
    {
        $this->qb = call_user_func($filters, $this->qb);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function applyOrderBy()
    {
        if ($sort = $this->gridSearchParametersBag->getSort()) {
            $index = 0;
            foreach ($sort as $sortItem) {
                $column = $this->mapping[$sortItem['field']];
                $dir = $sortItem['dir'];

                if ($column instanceof MappingColumn) {
                    $column = $column->getColumn();
                }

                $orderByMethod = 'addOrderBy';

                if ($index == 0) {
                    $orderByMethod = 'orderBy';
                }

                if (is_array($column)) {
                    foreach ($column as $col) {
                        $this->qb->$orderByMethod($col, strtoupper($dir));
                    }
                } else {
                    $this->qb->$orderByMethod($column, strtoupper($dir));
                }
                $index++;
            }
        } else if ($sortField = $this->gridSearchParametersBag->getSortField()) {
            $column = $this->mapping[$sortField];

            if ($column instanceof MappingColumn) {
                $column = $column->getColumn();
            }

            if (is_array($column)) {
                foreach ($column as $col) {
                    $this->qb->addOrderBy($col, strtoupper($this->gridSearchParametersBag->getSortDir()));
                }
            } else {
                $this->qb->orderBy($column, strtoupper($this->gridSearchParametersBag->getSortDir()));
            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function applyPagination()
    {
        $this->qb->setFirstResult($this->gridSearchParametersBag->getOffset());
        $this->qb->setMaxResults($this->gridSearchParametersBag->getLimit());

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function resetPaginationAndOrderBy()
    {
        $this->qb->resetDQLPart('orderBy');

        $this->qb
            ->setFirstResult(null)
            ->setMaxResults(null);

        return $this;
    }

    /**
     * @param array $filters
     * @param string $filtersLogic
     * @return $this
     * @throws \Exception
     */
    protected function createFiltersQuery(array $filters, $filtersLogic = 'and')
    {
        $expressions = [];
        foreach ($filters as $filter) {
            if (isset($filter['filters'])) {
                $this->createFiltersQuery($filter['filters'], (isset($filter['logic']) ? $filter['logic'] : 'and'));
            } else {
                $column = $this->mapping[$filter['field']];

                $value = $filter['value'];

                if (strpos($value, 'GMT') !== false) { // value can be a date
                    // Thu Nov 15 2012 00:00:00 GMT-0700 (Mountain Standard Time)
                    $date = \DateTime::createFromFormat('D M d Y H:i:s e+', $value);

                    if ($date instanceof \DateTime) {
                        $value = $date;
                    }
                }

                if ($value === 'true' || $value === 'false') {
                    $value = 'true' === $value ? true : false;
                }

                if ($column instanceof MappingColumn) {
                    if (is_array($column->getColumn())) {
                        if ($column->getIsConcatenation()) {
                            $subExpressions = array();
                            foreach ($column->getColumn() as $singleColumn) {
                                $subExpressions[] = $this->applyColumnExpression('concat', $singleColumn, $column->getConcatenationSeparator());
                            }

                            $concatenation = reset($subExpressions);
                            foreach ($subExpressions as $index => $subConcatenation) {
                                if ($index > 0) {
                                    $concatenation = $this->applyColumnExpression('concat', $concatenation, $subConcatenation);
                                }
                            }

                            $concatenation = $this->applyColumnExpression($filter['operator'], $concatenation, $value);

                            $expressions[] = new Expression($concatenation, $column->getIsVirtual());
                        } else {
                            $subExpressions = array();
                            foreach ($column->getColumn() as $singleColumn) {
                                $subExpressions[] = $this->applyColumnExpression($filter['operator'], $singleColumn, $value);
                            }

                            $expressions[] = new Expression($this->qb->expr()->orX(
                                $subExpressions[0], $subExpressions[1]
                            ), $column->getIsVirtual());
                        }
                    } else {
                        $expressions[] = new Expression($this->applyColumnExpression($filter['operator'], $column->getColumn(), $value), $column->getIsVirtual());
                    }
                } else {
                    //For mappings with simple array
                    if (is_array($column)) {
                        $subExpressions = array();
                        foreach ($column as $singleColumn) {
                            $subExpressions[] = $this->applyColumnExpression($filter['operator'], $singleColumn, $value);
                        }
                        $expressions[] = new Expression($this->qb->expr()->orX(
                            $subExpressions[0], $subExpressions[1]
                        ));
                    } else {
                        $expressions[] = new Expression($this->applyColumnExpression($filter['operator'], $column, $value));
                    }
                }
            }
        }

        if ('and' === $filtersLogic) {
            /** @var $expression Expression */
            foreach ($expressions as $expression) {
                if ($expression->getIsVirtual()) {
                    $this->qb->andHaving($expression->getExpr());
                } else {
                    $this->qb->andWhere($expression->getExpr());
                }
            }
        } elseif ('or' === $filtersLogic) {
            $orX = $this->qb->expr()->orX();

            /** @var $expression Expression */
            foreach ($expressions as $expression) {
                $orX->add($expression->getExpr());
            }

            if ($expression->getIsVirtual()) {
                $this->qb->andHaving($orX);
            } else {
                $this->qb->andWhere($orX);
            }
        }

        return $this;
    }

    /**
     * @param $operator
     * @param $column
     * @param string $value
     * @return \Doctrine\ORM\Query\Expr\Comparison
     * @throws \Exception
     */
    protected function applyColumnExpression($operator, $column, $value)
    {
        $paramName = preg_replace('/[^A-Za-z0-9 ]/', '', $column) . uniqid();
        switch ($operator) {
            case 'eq':
                if (empty($value)) {
                    $expr = $this->qb->expr()->orX(
                        $this->qb->expr()->eq($column, ':' . $paramName),
                        $this->qb->expr()->isNull($column)
                    );
                } else {
                    $expr = $this->qb->expr()->eq($column, ':' . $paramName);
                }
                
                $this->qb->setParameter($paramName, $value);
                break;
            case 'neq':
                $expr = $this->qb->expr()->orX(
                    $this->qb->expr()->neq($column, ':' . $paramName),
                    $this->qb->expr()->isNull($column)
                );

                $this->qb->setParameter($paramName, $value);
                break;
            case 'gt':
                $expr = $this->qb->expr()->gt($column, ':' . $paramName);
                $this->qb->setParameter($paramName, $value);
                break;
            case 'lt':
                $expr = $this->qb->expr()->lt($column, ':' . $paramName);
                $this->qb->setParameter($paramName, $value);
                break;
            case 'startswith':
                $expr = $this->qb->expr()->like($column, $this->qb->expr()->literal((string)$value . '%'));
                break;
            case 'contains':
                $expr = $this->qb->expr()->like($column, $this->qb->expr()->literal('%' . (string)$value . '%'));
                break;
            case 'doesnotcontain':
                $expr = $this->qb->expr()->notLike($column, $this->qb->expr()->literal('%' . (string)$value . '%'));
                break;
            case 'endswith':
                $expr = $this->qb->expr()->like($column, $this->qb->expr()->literal('%' . (string)$value));
                break;
            case 'concat':
                if (is_string($value)) {
                    $value = $this->qb->expr()->literal($value);
                }

                $expr = $this->qb->expr()->concat(sprintf('COALESCE(%s, \'\')', $column), $value);
                break;
            case 'isnull':
                $expr = $this->qb->expr()->orX(
                    $this->qb->expr()->isNull($column),
                    $this->qb->expr()->eq($column, $this->qb->expr()->literal(''))
                );
                break;
            case 'isnotnull':
                $expr = $this->qb->expr()->andX(
                    $this->qb->expr()->isNotNull($column),
                    $this->qb->expr()->neq($column, $this->qb->expr()->literal(''))
                );
                break;
            default:
                throw new \Exception(sprintf('Operator "%s" is not supported', $operator));
        }

        return $expr;
    }
}