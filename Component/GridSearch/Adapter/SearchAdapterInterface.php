<?php

namespace SDI\ComponentsBundle\Component\GridSearch\Adapter;

use Closure;

interface SearchAdapterInterface
{
    /**
     * Get data. It is not apply order and pagination
     *
     * @return array|null
     */
    public function getData();

    /**
     * Get data. Each row has column exactly the same like in the mapping
     *
     * @return array|null
     */
    public function getDataWithColumnsFromMapping();

    /**
     * Get data applying order and pagination
     *
     * @return array|null
     */
    public function getGridData();

    /**
     * Get amount of result
     *
     * @return int
     */
    public function getTotal();

    /**
     * Set mapping
     *
     * @param array $mapping
     * @return $this
     */
    public function setMapping($mapping);

    /**
     * @return array
     */
    public function getMapping();

    /**
     * @return $this|void
     */
    public function applyFilters();

    /**
     * @param Closure $closure
     * @return $this
     */
    public function applyNextFilter(Closure $closure);

    /**
     * @return $this
     */
    public function applyOrderBy();

    /**
     * @return $this
     */
    public function applyPagination();

    /**
     * Reset orderBy and clears pagination parameters
     *
     * @return $this
     */
    public function resetPaginationAndOrderBy();
}