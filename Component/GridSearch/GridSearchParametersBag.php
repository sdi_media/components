<?php

namespace SDI\ComponentsBundle\Component\GridSearch;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class GridSearchParametersBag implements GridSearchParametersBagInterface
{
    private $limit;

    private $offset;

    private $sortField;

    private $sortDir;

    private $sort;

    private $filters;

    private $filtersLogic;

    public function __construct(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();
        $this->parseRequest($request);
    }

    /**
     * @param Request $request
     */
    protected function parseRequest($request)
    {
        $params = $request->request;
        $getParams = $request->query;
        $params->add($getParams->all());
        $page = intval($params->get('page'));
        $rows = intval($params->get('take'));
        $this->limit = $rows;
        $this->offset = ($page - 1) * $rows;

        $sortParams = $params->get('sort');
        if (count($sortParams) > 1) {
            $this->sort = $sortParams;
        } else if (count($sortParams)) {
            $this->sortField = $sortParams[0]['field'];
            $this->sortDir = $sortParams[0]['dir'];
        }

        $filterParams = $params->get('filter');

        if ($filterParams && count($filterParams) && isset($filterParams['filters'])) {
            $this->filters = $filterParams['filters'];
            $this->filtersLogic = $filterParams['logic'];
        } else {
            $this->filters = array();
        }
    }

    /**
     *
     * @param Request $request
     * @return bool
     */
    public static function handleIsActiveSearchFilter(Request $request)
    {
        $requestArray = $request->request->all();
        $active = true;

        if (isset($requestArray['filter']) && $requestArray['filter']) {
            foreach ($requestArray['filter']['filters'] as $singleFilter) {
                if (isset($singleFilter['field']) && $singleFilter['field'] === 'is_active' && $singleFilter['value'] === 'false') {
                    $active = false;
                }
            }
        }

        return $active;
    }	
	
    /**
     * @return integer
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return integer
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @return string
     */
    public function getSortDir()
    {
        return $this->sortDir;
    }

    /**
     * @return string
     */
    public function getSortField()
    {
        return $this->sortField;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @return string
     */
    public function getFiltersLogic()
    {
        return $this->filtersLogic;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'limit' => $this->limit,
            'offset' => $this->offset,
            'sortDir' => $this->sortDir,
            'sortField' => $this->sortField,
            'filters' => $this->filters,
            'filtersLogic' => $this->filtersLogic,
            'sort' => $this->sort
        ];
    }
}
