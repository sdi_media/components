<?php

namespace SDI\ComponentsBundle\Component\GridSearch;

/**
 * Class MappingColumn
 * @package SDI\ComponentsBundle\Component\GridSearch
 */
class MappingColumn
{
    protected $column;
    protected $isVirtual;
    protected $isConcatenation;
    protected $concatenationSeparator = '';

    /**
     * @param $column
     * @param bool|false $isVirtual
     * @param bool|false $isConcatenation
     * @param string $concatenationSeparator
     */
    public function __construct($column, $isVirtual = false, $isConcatenation = false, $concatenationSeparator = '')
    {
        $this->column = $column;
        $this->isVirtual = $isVirtual;
        $this->isConcatenation = $isConcatenation;
        $this->concatenationSeparator = $concatenationSeparator;

        return $this;
    }

    /**
     * @param mixed $column
     */
    public function setColumn($column)
    {
        $this->column = $column;
    }

    /**
     * @return mixed
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * @param boolean $isVirtual
     */
    public function setIsVirtual($isVirtual)
    {
        $this->isVirtual = $isVirtual;
    }

    /**
     * @return boolean
     */
    public function getIsVirtual()
    {
        return $this->isVirtual;
    }

    /**
     * @return mixed
     */
    public function getIsConcatenation()
    {
        return $this->isConcatenation;
    }

    /**
     * @return string
     */
    public function getConcatenationSeparator()
    {
        return $this->concatenationSeparator;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        if (is_array($this->column)) {
            return reset($this->column);
        }

        return sprintf('%s', $this->column);
    }
}
