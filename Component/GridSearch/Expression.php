<?php

namespace SDI\ComponentsBundle\Component\GridSearch;

/**
 * Class Expression
 * @package SDI\ComponentsBundle\Component\GridSearch
 */
class Expression
{
    private $expr;
    private $isVirtual;

    public function __construct($expr, $isVirtual = false)
    {
        $this->expr = $expr;
        $this->isVirtual = $isVirtual;
    }

    /**
     * @param mixed $expr
     */
    public function setExpr($expr)
    {
        $this->expr = $expr;
    }

    /**
     * @return mixed
     */
    public function getExpr()
    {
        return $this->expr;
    }

    /**
     * @param mixed $isVirtual
     */
    public function setIsVirtual($isVirtual)
    {
        $this->isVirtual = $isVirtual;
    }

    /**
     * @return mixed
     */
    public function getIsVirtual()
    {
        return $this->isVirtual;
    }
}
