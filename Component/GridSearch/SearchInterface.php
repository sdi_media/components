<?php
namespace SDI\ComponentsBundle\Component\GridSearch;

use SDI\ComponentsBundle\Component\GridSearch\Adapter\SearchAdapterInterface;

interface SearchInterface
{
    /**
     * @param SearchAdapterInterface $adapter
     * @return $this
     */
    public function setAdapter(SearchAdapterInterface $adapter);

    /**
     * @return SearchAdapterInterface
     */
    public function getAdapter();

    public function getGridData();
}