<?php

namespace SDI\ComponentsBundle\Component\WebAuth;

use SDI\ComponentsBundle\Component\WebAuth\Response\RegisterUser;
use SDI\ComponentsBundle\Component\WebAuth\Response\TicketValidation;
use Symfony\Component\DomCrawler\Crawler;

class WebAuthService
{
    private $secret;

    private $sourceIp;

    private $authUrl;

    private $service;

    public function __construct($secret, $sourceIp, $authUrl, $service)
    {
        $this->secret = $secret;
        $this->sourceIp = $sourceIp;
        $this->authUrl = $authUrl;
        $this->service = $service;
    }

    /**
     * @param string $url
     * @return boolean
     */
    protected function sendWebAuthRequest($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);

        $response = curl_exec($ch);
        $error = curl_errno($ch);

        $responseHttpCode = intval(curl_getinfo($ch, CURLINFO_HTTP_CODE));

        curl_close($ch);

        if ($responseHttpCode !== 200 || $error) {
            return null;
        }

        return preg_split("/\n/", $response);
    }

    /**
     * @param string $ticketHash
     * @return TicketValidation|null
     */
    public function validateTicket($ticketHash)
    {
        $requestArgs = array(
            'service' => $this->service,
            'ticket_hash' => $ticketHash,
            'digest' => $this->digest($ticketHash)
        );
        $url = $this->authUrl . '/service/present_ticket?' . http_build_query($requestArgs);

        $requestResponse = $this->sendWebAuthRequest($url);

        if (null !== $requestResponse) {
            $xmlContent = trim(implode('', $requestResponse));
            $domCrawler = new Crawler();
            $domCrawler->addXmlContent($xmlContent);

            $responseCrawler = $domCrawler->filterXPath('//Response');

            if ($responseCrawler->count() && $responseCrawler->text() == 'OK') {
                return new TicketValidation(
                    $domCrawler->filterXPath('//AccountId')->text(),
                    $domCrawler->filterXPath('//Username')->text(),
                    $domCrawler->filterXPath('//Fullname')->text(),
                    $domCrawler->filterXPath('//IPAddress')->text()
                );
            }
        }

        return null;
    }

    /**
     * @param string $email
     * @param string $fullName
     * @return RegisterUser|null
     */
    public function registerUser($email, $fullName, $sendInvitationEmail = true)
    {
        $payload = $email . $fullName;

        $requestArgs = array(
            'service' => $this->service,
            'email' => $email,
            'full_name' => $fullName,
            'digest' => $this->digest($payload)
        );

        if (!$sendInvitationEmail) {
            $requestArgs['send_invitation_email'] = 'false';
        }

        $url = $this->authUrl . '/service/register?' . http_build_query($requestArgs);
        $requestResponse = $this->sendWebAuthRequest($url);

        if (null !== $requestResponse) {
            $xmlContent = trim(implode('', $requestResponse));
            $domCrawler = new Crawler();
            $domCrawler->addXmlContent($xmlContent);

            $responseCrawler = $domCrawler->filterXPath('//Response');

            if ($responseCrawler->count() && $responseCrawler->text() == 'OK') {
                return new RegisterUser(
                    $domCrawler->filterXPath('//AccountId')->text()
                );
            }
        }

        return null;
    }

    /**
     * @param $id
     * @return null|string
     */
    public function unlockAccount($id)
    {
        $args = array(
            'service' => $this->service,
            'account_id' => $id,
            'digest' => $this->digest($id)
        );

        $url = $this->authUrl . "/service/unlock_account?" . http_build_query($args);
        $requestResponse = $this->sendWebAuthRequest($url);

        if (null !== $requestResponse) {
            $xmlContent = trim(implode('', $requestResponse));
            $domCrawler = new Crawler();
            $domCrawler->addXmlContent($xmlContent);

            $responseCrawler = $domCrawler->filterXPath('//Response');

            if ($responseCrawler->count() && $responseCrawler->text() == 'OK') {
                return $responseCrawler->text();
            }
        }

        return null;
    }

    /**
     * @param $id
     * @return null|string
     */
    public function deleteAccount($id)
    {
        $args = array(
            'service' => $this->service,
            'account_id' => $id,
            'digest' => $this->digest($id)
        );

        $url = $this->authUrl . "/service/delete_account?" . http_build_query($args);
        $requestResponse = $this->sendWebAuthRequest($url);

        if (null !== $requestResponse) {
            $xmlContent = trim(implode('', $requestResponse));
            $domCrawler = new Crawler();
            $domCrawler->addXmlContent($xmlContent);

            $responseCrawler = $domCrawler->filterXPath('//Response');

            if ($responseCrawler->count() && $responseCrawler->text() == 'OK') {
                return $responseCrawler->text();
            }
        }

        return null;
    }

    /**
     * @param string $email
     * @param string $fullName
     * @return null|string
     */
    public function checkAccountExists($email, $fullName)
    {
        $args = array(
            'full_name' => $fullName,
            'email' => $email,
            'digest' => $this->digest($email . $fullName)
        );

        $url = $this->authUrl . "/api/find_user?" . http_build_query($args);
        $requestResponse = $this->sendWebAuthRequest($url);

        if (null !== $requestResponse) {
            $str = preg_replace('/^[\pZ\p{Cc}\x{feff}]+|[\pZ\p{Cc}\x{feff}]+$/ux', '', $requestResponse[0]);
            return json_decode($str);
        }

        return null;
    }

    /**
     * @param $username
     * @param $password
     * @return mixed|null
     */
    public function login($username, $password)
    {
        $args = array(
            'username' => $username,
            'password' => $password,
            'digest' => $this->digest($username . $password)
        );

        $url = $this->authUrl . "/api/login?" . http_build_query($args);
        $requestResponse = $this->sendWebAuthRequest($url);

        if (null !== $requestResponse) {
            $str = preg_replace('/^[\pZ\p{Cc}\x{feff}]+|[\pZ\p{Cc}\x{feff}]+$/ux', '', $requestResponse[0]);
            return json_decode($str);
        }

        return null;
    }

    /**
     * @param string $payload
     * @return string
     */
    private function digest($payload)
    {
        return (sha1($payload . $this->sourceIp . $this->secret));
    }
}