<?php

namespace SDI\ComponentsBundle\Component\WebAuth;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserChecker;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\HttpUtils;

class Authenticator implements SimplePreAuthenticatorInterface, AuthenticationSuccessHandlerInterface
{
    protected $userProvider;

    protected $userChecker;

    protected $httpUtils;

    protected $targetPath;

    public function __construct(TicketHashUserProvider $userProvider, UserChecker $userChecker, HttpUtils $httpUtils, $targetPath)
    {
        $this->userProvider = $userProvider;
        $this->userChecker = $userChecker;
        $this->httpUtils = $httpUtils;
        $this->targetPath = $targetPath;
    }

    public function createToken(Request $request, $providerKey)
    {
        // look for an apikey query parameter
        $ticketHash = $request->query->get('ticket');

        if (!$ticketHash) {
            throw new BadCredentialsException('No API key found');
        }

        return new PreAuthenticatedToken(
            'anon.',
            $ticketHash,
            $providerKey
        );
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $ticketHash = $token->getCredentials();
        $username = $this->userProvider->getUsernameForTicketHash($ticketHash);

        // User is the Entity which represents your user
        $user = $token->getUser();

        $userEntityClass = $this->userProvider->getUserEntityClass();

        if ($user instanceof $userEntityClass) {
            $this->userChecker->checkPreAuth($user);
            return new PreAuthenticatedToken(
                $user,
                $ticketHash,
                $providerKey,
                $user->getRolesArray()
            );
        }

        if (!$username) {
            throw new AuthenticationException(
                sprintf('Ticket hash "%s" does not exist.', $ticketHash)
            );
        }

        $user = $this->userProvider->loadUserByUsername($username);
        $this->userChecker->checkPreAuth($user);

        return new PreAuthenticatedToken(
            $user,
            $ticketHash,
            $providerKey,
            $user->getRolesArray()
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    /**
     * This is called when an interactive authentication attempt succeeds. This
     * is called by authentication listeners inheriting from
     * AbstractAuthenticationListener.
     *
     * @param Request $request
     * @param TokenInterface $token
     *
     * @return Response never null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $requestUri = str_replace('?' . $request->getQueryString(), '', $request->getRequestUri());
        $targetPath = sprintf('%s?continue=%s', $this->targetPath, $requestUri);

        return $this->httpUtils->createRedirectResponse($request, $targetPath);
    }
}