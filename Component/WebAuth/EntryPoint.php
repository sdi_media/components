<?php

namespace SDI\ComponentsBundle\Component\WebAuth;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Component\Security\Http\HttpUtils;

class EntryPoint implements AuthenticationEntryPointInterface
{
    protected $httpUtils;

    protected $loginPath;

    public function __construct(HttpUtils $httpUtils, $loginPath)
    {
        $this->httpUtils = $httpUtils;
        $this->loginPath = $loginPath;
    }

    /**
     * Starts the authentication scheme.
     *
     * @param Request $request The request that resulted in an AuthenticationException
     * @param AuthenticationException $authException The exception that started the authentication process
     *
     * @return Response
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $loginPath = sprintf("%s/?continue=%s", $this->loginPath, $request->getUri());
        return $this->httpUtils->createRedirectResponse($request, $loginPath);
    }
}