<?php

namespace SDI\ComponentsBundle\Component\WebAuth\Response;

class TicketValidation
{
    private $accountId;

    private $username;

    private $fullName;

    private $ipAddress;

    public function __construct($accountId, $username, $fullName, $ipAddress)
    {
        $this->accountId = $accountId;
        $this->username = $username;
        $this->fullName = $fullName;
        $this->ipAddress = $ipAddress;
    }

    /**
     * @return string
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }
}