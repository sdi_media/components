<?php

namespace SDI\ComponentsBundle\Component\WebAuth\Response;

class RegisterUser
{
    private $accountId;

    public function __construct($accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return string
     */
    public function getAccountId()
    {
        return $this->accountId;
    }
}
