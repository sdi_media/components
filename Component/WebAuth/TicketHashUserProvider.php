<?php

namespace SDI\ComponentsBundle\Component\WebAuth;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class TicketHashUserProvider implements UserProviderInterface
{
    /**
     * @var WebAuthService
     */
    protected $webAuthService;

    /**
     * @var ObjectManager
     */
    protected $entityManager;

    private $ticketHash;

    private $userEntityClass;

    public function __construct(WebAuthService $webAuthService, $entityManager, $userEntityClass)
    {
        $this->webAuthService = $webAuthService;
        $this->entityManager = $entityManager;
        $this->userEntityClass = $userEntityClass;
    }

    /**
     * Get proper
     *
     * @param string $ticketHash
     * @return string|null
     */
    public function getUsernameForTicketHash($ticketHash)
    {
        $this->ticketHash = $ticketHash;
        $ticketValidationResponse = $this->webAuthService->validateTicket($this->ticketHash);

        if (null === $ticketValidationResponse) {
            return null;
        }

        return $ticketValidationResponse->getUsername();
    }

    /**
     * Load user from database
     *
     * @param string $username
     * @return User
     * @throws UsernameNotFoundException
     */
    public function loadUserByUsername($username)
    {
        $user = $this->entityManager->getRepository($this->userEntityClass)->findOneBy(array('username' => $username));

        if ($user instanceof $this->userEntityClass) {
            return $user;
        }

        throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $username)
        );
    }

    /**
     * Refresh user
     *
     * @param UserInterface $user
     * @return User
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof $this->userEntityClass) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Check whether class is supported 
     *
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return $this->userEntityClass === $class;
    }

    /**
     * @return mixed
     */
    public function getUserEntityClass()
    {
        return $this->userEntityClass;
    }
}