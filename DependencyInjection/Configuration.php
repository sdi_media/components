<?php

namespace SDI\ComponentsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface {

    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder() {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('sdi_components');
        $rootNode
            ->children()
                ->arrayNode('web_auth')
                    ->isRequired()
                    ->children()
                        ->scalarNode('service')
                            ->defaultValue('')
                        ->end()
                        ->scalarNode('secret')
                            ->defaultValue('ppp')
                        ->end()
                        ->scalarNode('source_ip')
                            ->defaultValue('')
                        ->end()
                        ->scalarNode('url')
                            ->defaultValue('https://webauth.mixana.com')
                        ->end()
                        ->scalarNode('target_path')
                            ->defaultValue('/')
                        ->end()
                        ->scalarNode('user_class')
                            ->defaultValue('')
                        ->end()
                    ->end()
                ->end()
            ->end()
        ->end();

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }

}
